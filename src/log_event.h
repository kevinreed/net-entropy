#ifndef LOG_EVENT_H
#define LOG_EVENT_H

void
log_rising_alarm(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e);
void
log_falling_alarm(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e);
void
log_max_data(struct tcp_stream *a_tcp, stats_t *stats);
void
log_out_of_range_alarm(struct tcp_stream *a_tcp, stats_t *stats);
void
log_reenter_range(struct tcp_stream *a_tcp, stats_t *stats);
void
log_end_of_connection(struct tcp_stream *a_tcp, stats_t *stats);
void
log_new_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e);
void
log_update_min_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e);
void
log_update_max_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e);

#ifdef ENABLE_TIMEOUT
void
log_timeout(struct tcp_stream *a_tcp, stats_t *stats);
#endif /* ENABLE_TIMEOUT */

#endif /* LOG_EVENT_H */
