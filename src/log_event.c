/**
 ** @file log_event.c
 ** Functions for logging events reported by Net-Entropy.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Mon Oct 19 22:51:32 2009
 ** @date Last update: Mon Oct 19 22:51:32 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <syslog.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <nids.h>

#include "net-entropy.h"

#include "log.h"
#include "port_track.h"

#include "log_event.h"

/**
 * Rise an entropy alarm on a connection.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 * @param e      Entropy at the moment of the alarm.
 **/
void
log_rising_alarm(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_warning("RISING ALARM on %s:%i -> %s:%i "
              "offset=%i packets=%i entropy=%10.8f range=%i (%li %li %f %f)\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, stats->packets, e,
              stats->cur_range,
              z->start, z->end, z->ent_min, z->ent_max);
}



/**
 * Fall an entropy alarm on a connection.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 * @param e      Entropy at the moment of the falling alarm.
 **/
void
log_falling_alarm(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_warning("Falling alarm on %s:%i -> %s:%i "
              "offset=%i packets=%i entropy=%10.8f range=%i (%li %li %f %f)\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, stats->packets, e,
              stats->cur_range,
              z->start, z->end, z->ent_min, z->ent_max);
}



/**
 * Notice that a connection that have made at least one alarm will not
 * be analyzed anymore.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 **/
void
log_max_data(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  log_warning("Stop tracking %s:%i -> %s:%i "
              "offset=%i > maxdata=%i packets=%i\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, max_tracking_size_g, stats->packets);
}



/**
 * Notice that a connection that have made at least one alarm has terminated.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 **/
void
log_end_of_connection(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];
  char *reason;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  reason = "unknown reason";
  if (a_tcp->nids_state == NIDS_CLOSE) {
    reason = "connection closed";
  }
  else if (a_tcp->nids_state == NIDS_RESET) {
    reason = "connection reset";
  }
  else if (a_tcp->nids_state == NIDS_EXITING) {
    reason = "end of capture";
  }

  log_warning("End of connection %s:%i -> %s:%i offset=%i packets=%i (%s)\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, stats->packets, reason);
}



/**
 * Notice that a connection reached an undefined range in a rule.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 **/
void
log_out_of_range_alarm(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  log_warning("Out of range %s:%i -> %s:%i offset=%i packets=%i\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, stats->packets);
}



/**
 * Notice that a connection that have reached an undefined range in a
 * rule, reenter in a defined range.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 **/
void
log_reenter_range(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_warning("Re-enter range %s:%i -> %s:%i offset=%i packets=%i "
              "range=%i (%li %li %f %f)\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              stats->bytes, stats->packets,
              stats->cur_range,
              z->start, z->end, z->ent_min, z->ent_max);
}



/**
 * Notify that a new value has been learned.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 * @param e      Entropy at the moment message.
 **/
void
log_new_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_notice("Learned new value %s:%i -> %s:%i offset=%i packets=%i "
             "range=%i (%li %li %f %f) new_val=%f\n",
             src_ip, a_tcp->addr.source,
             dst_ip, a_tcp->addr.dest,
             stats->bytes, stats->packets,
             stats->cur_range,
             z->start, z->end, z->ent_min, z->ent_max,
             e);
}



/**
 * Notify that a minimal value has been updated.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 * @param e      Entropy at the moment message.
 **/
void
log_update_min_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_notice("Learned new min value %s:%i -> %s:%i offset=%i packets=%i "
             "range=%i (%li %li %f %f) new_min=%f\n",
             src_ip, a_tcp->addr.source,
             dst_ip, a_tcp->addr.dest,
             stats->bytes, stats->packets,
             stats->cur_range,
             z->start, z->end, z->ent_min, z->ent_max,
             e);
}



/**
 * Notify that a maximal value has been updated.
 *
 * @param a_tcp  The current libnids TCP stream.
 * @param stats  Statistics of the current stream.
 * @param e      Entropy at the moment message.
 **/
void
log_update_max_value(struct tcp_stream *a_tcp, stats_t *stats, entropy_t e)
{
  char src_ip[16];
  char dst_ip[16];
  range_t *z;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  z = &ptbl_g[ a_tcp->addr.dest ].ranges[ stats->cur_range ];

  log_notice("Learned new max value %s:%i -> %s:%i offset=%i packets=%i "
             "range=%i (%li %li %f %f) new_max=%f\n",
             src_ip, a_tcp->addr.source,
             dst_ip, a_tcp->addr.dest,
             stats->bytes, stats->packets,
             stats->cur_range,
             z->start, z->end, z->ent_min, z->ent_max,
             e);
}


#ifdef ENABLE_TIMEOUT
/**
 * Log when a connection that have made at least one alert is inactive
 * during a while.
 *
 * @param a_tcp   The current libnids TCP stream.
 * @param stats   Statistics for the current TCP stream.
 **/
void
log_timeout(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  log_warning("Stop tracking %s:%i -> %s:%i "
              "last_act=%li > timeout=%li offset=%i packets=%i\n",
              src_ip, a_tcp->addr.source,
              dst_ip, a_tcp->addr.dest,
              (long int)(last_gc_g - stats->last_act), (long int)timeout_g,
              stats->bytes, stats->packets);
}
#endif /* ENABLE_TIMEOUT */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
