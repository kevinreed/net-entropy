#ifndef CONN_TIMEOUT_H
#define CONN_TIMEOUT_H

typedef DTAILQ_HEAD(con_list, stats_t) con_list_t;

extern con_list_t con_list_g;

int
destroy_inactive_connections(time_t curtime, struct tcp_stream *a_tcp);

#endif /* CONN_TIMEOUT_H */
