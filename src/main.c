/**
 ** @file net-entropy.c
 ** Net-entropy: TCP Connection entropy checker.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Wed Mar 17 02:13:15 2004
 ** @date Last update: Sat Nov 28 19:53:05 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <syslog.h>
#include <nids.h>

#include "net-entropy.h"

#include "dtailq.h"
#include "paninski.h"
#include "int_range.h"
#include "log.h"
#include "port_track.h"
#include "log_event.h"
#include "cfg_cmdline.h"
#include "cfg_file.h"
#include "conn_stats.h"
#include "nids_callback.h"
#include "misc.h"

/** @var config_file_g
 ** Configuration file to use.
 **/
/** @var verbose_g
 ** Enable verbose mode if not set to 0.
 **/
/** @var daemon_g
 ** Enable/disable daemon mode.
 **/
/** @var learn_mode_g
 ** Enable/disable laerning mode.
 **/
/** @var delete_old_rules_g
 ** Enable/disable backup of rule when updated.
 **/
/** @var ports_g
 ** Number of protocol to analyse.
 **/
/** @var runtime_user_g
 ** The user id to get when droopping privileges.
 **/
/** @var priority_g
 ** The priority that will be requested for the process.  This may
 ** prevent Net-Entropy of eating all the CPU resources in case of a
 ** problem (high load, denial of service attempt).
 **/
/**  @var memory_limit_g
 **    The memory limit.
 **/
/**  @var flush_stats_g
 **    Flush statistics stream at each line if set to non-zero.  Allow
 **    to follow connections delayed in time.
 **/
/**  @var byte_stat_g
 **    Write a statistic line at each byte instead of each packet.  Write
 **  more precise but bigger statistics files.
 **/
/**  @var statdir_g
 **    Directory where statistic files will be written.
 **    This directory must be accessible to the user defined in runtime_user_g.
 **/
/**  @var force_syslog_g
 **    Tell to log via syslog().  This flag is usefull only to override the
 **    default behaviour of the foreground mode which will log to the standard
 **    error stream.
 **/
/**  @var timeout_g
 **    The timeout of connections, in seconds.  This is also the frequency
 **    of garbage collecting.
 **/
/**  @var last_gc_g
 **    Date of the last garbage collecting.
 **/
/**  @var con_list_g
 **    List of the current active session being analyzed by net-entropy.
 **/
/* Globals */
char *config_file_g = DEFAULT_CONFIG_FILE;
int verbose_g = 0;
int daemon_g = DAEMON_REQUESTED;
int learn_mode_g = 0;
int delete_old_rules_g = 0;
int ports_g = 0;
char *runtime_user_g = NULL;
int priority_g = 0;
int memory_limit_g = 0;
int max_tracking_size_g = MAX_TRACKING_SIZE;
int force_syslog_g = 0;
#ifdef ENABLE_STATISTICS
int flush_stats_g = 0;
int byte_stat_g = 0;
char *statdir_g = NULL;
#endif /* ENABLE_STATISTICS */



/**
 * Main function.
 * Prepare execution context, initialise default values,
 * parse command line options and enter in the main loop.
 * @param argc argument counter
 * @param argv argument vector
 **/
int
main(int argc, char *argv[])
{
  nids_params.scan_num_hosts = 0;

  init_port_track_table();
  parse_cmdline(argc, argv);
  read_config_file(config_file_g);

  if (memory_limit_g > 0)
    set_memory_limit(memory_limit_g);

  if (ports_g == 0) {
    log_error("no server port to track.\n");
    net_ent_usage();
    exit(EXIT_FAILURE);
  }

  if (verbose_g)
    fprintf_port_track_table(stderr);

  if ( !nids_init() ) {
    log_error("nids_init(): %s\n", nids_errbuf);
    exit(EXIT_FAILURE);
  }

  nids_register_tcp(tcp_callback);

  if (daemon_g == DAEMON_REQUESTED)
    daemonize(NULL, NULL);

  if ((daemon_g == DAEMON_REQUESTED) || force_syslog_g)
    set_log_error_output(LOG_ERROR_SYSLOG);

  if (priority_g != 0)
    set_process_priority(priority_g);

#ifdef ENABLE_PANINSKI
  paninski_init_cache();
#endif /* ENABLE_PANINSKI */

  if (runtime_user_g)
    change_run_id(runtime_user_g);

  openlog(NETENT_SYSLOG_IDENT, LOG_NDELAY | LOG_PID, LOG_USER);
  syslog(LOG_INFO, "Starting net-entropy daemon");

  if (learn_mode_g && nids_params.filename == NULL) {
    log_error("warning: on-line learning mode: "
              "make sure to be on a safe network\n");
  }

  nids_run();

  return (0);
}



/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
