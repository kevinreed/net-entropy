/**
 ** @file net-entropy.h
 ** Net-entropy header file.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Wed Mar 17 02:20:35 2004
 ** @date Last update: Thu Dec  5 00:16:46 2013
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifndef NET_ENTROPY_H
#define NET_ENTROPY_H

#include <sys/types.h>
#include <math.h>

#include "dtailq.h"
#include "int_range.h"

/**
 ** @def MAX_TRACKING_SIZE
 **   Define the default maximum amount of data analyzed in eah connections.
 **/
#define MAX_TRACKING_SIZE (64*1024)

/**
 ** @def MAX_PORTS
 **   The number of ports in TCP.
 **/
#define MAX_PORTS 65536

/**
 ** @def TRACK_NOTHING
 **   Analysis type: no anylysis.
 **/
#define TRACK_NOTHING 0

/**
 ** @def TRACK_BYTES
 **   Analysis type: data size ranges are given in bytes.
 **/
#define TRACK_BYTES 1

/**
 ** @def TRACK_PACKETS
 **   Analysis type: data size ranges are given in packets.
 **/
#define TRACK_PACKETS 2

/**
 ** @def TRACK_TYPE_MASK
 **   Bit mask for analysis type.
 **/
#define TRACK_TYPE_MASK (0x03)

/**
 ** @def TRACK_TYPE
 **   A macro that only mask the analysis type.
 **/
#define TRACK_TYPE(x) ( (x) & TRACK_TYPE_MASK )

/**
 ** @def TRACK_PERPACKET
 **   Disable the cumulative mode: computes and check entropy of each packet.
 **/
#define TRACK_PERPACKET  (0 << 2)

/**
 ** @def TRACK_CUMULATIVE
 **   Enable the cumulative mode: computes and check entropy of the whole
 **   tcp connection.
 **/
#define TRACK_CUMULATIVE (1 << 2)

/**
 ** @def TRACK_CUMUL_MASK
 **   Bit mask for the cumulative mode.
 **/
#define TRACK_CUMUL_MASK (0x04)

/**
 ** @def TRACK_IS_CUMUL
 **   A macro that only masks the cumulative mode.
 **/
#define TRACK_IS_CUMUL(x) ( (x) & TRACK_CUMUL_MASK )


/**
 ** @def TRACK_CLI2SRV
 **   Data direction analysis: only analyze data from client to server.
 **/
#define TRACK_CLI2SRV  (1 << 4)

/**
 ** @def TRACK_SRV2CLI
 **   Data direction analysis: only analyze data from server to client.
 **/
#define TRACK_SRV2CLI  (2 << 4)

/**
 ** @def TRACK_BOTH
 **   Data direction analysis: analyze all data between the client and
 **   the server.
 **/
#define TRACK_BOTH    (TRACK_CLI2SRV|TRACK_SRV2CLI)

/**
 ** @def TRACK_DIR_MASK
 **   A bit mask for the data direction to analyze.
 **/
#define TRACK_DIR_MASK (0x30)

/**
 ** @def TRACK_DIR
 **   A macro that only masks the direction.
 **/
#define TRACK_DIR(x)  ( (x) & TRACK_DIR_MASK )

/**
 ** @def TRACK_SHANNON
 **   Use the Claude E. Shannon's entropy estimator.
 **/
#define TRACK_SHANNON  (0 << 6)

/**
 ** @def TRACK_PANINSKI
 **   Use the Liam Paninski's entropy estimator.
 **/
#define TRACK_PANINSKI (1 << 6)

/**
 ** @def TRACK_ESTIMATOR_MASK
 **   A bit mask for the entropy estimator to use.
 **/
#define TRACK_ESTIMATOR_MASK (0x40)

/**
 ** @def TRACK_ESTIMATOR
 **   A macro that only masks the entropy estimator.
 **/
#define TRACK_ESTIMATOR(x)  ( (x) & TRACK_ESTIMATOR_MASK )

/**
 ** @def DEFAULT_TRACK_TYPE
 **   Definition of the default analysis mode: Bytes,
 **   Cumulative, Both directions.
 **/
#define DEFAULT_TRACK_TYPE (TRACK_BYTES|TRACK_CUMULATIVE|TRACK_BOTH|TRACK_SHANNON)

/**
 ** @def NETENT_SYSLOG_IDENT
 **   Process identification string for the syslog() call.
 **/
#define NETENT_SYSLOG_IDENT "net-entropy"

/**
 ** @def DAEMON_NO
 **   Do not detach deamon for running (for debugging).
 **/
#define DAEMON_NO        0

/**
 ** @def DAEMON_REQUESTED
 **   Daemon mode have been requested but is not detached.
 **/
#define DAEMON_REQUESTED 1

/**
 ** @def DAEMON_RUNNING
 **   Daemon mode have been requested and is detached.
 **/
#define DAEMON_RUNNING   2

/**
 ** @def ALARM_ENTROPY
 **   The connection have generated an entropy alarm.
 **/
#define ALARM_ENTROPY   (1 << 0)

/**
 ** @def ALARM_RANGE
 **   The connection have generated a range alarm.
 **   A range alarm is emitted when then connection is in an undefined range.
 **/
#define ALARM_RANGE     (1 << 1)


/**
 ** @def ALARM_UPDATE
 **   The connection have generated an update message.
 **/
#define ALARM_UPDATE    (1 << 16)

/**
 ** @def M_LN2
 **   The constant value log(2.0).  This value is usually defined in math.h.
 **   It is defined only here if it's not available in math.h.
 **/
#ifndef M_LN2
#define M_LN2 0.69314718055994530942
#endif /* M_LN2 */

/**
 ** @def LOG2(x)
 **   Computes binary logarithm.
 ** @param x
 **   Value.
 **/
#ifdef ENABLE_DOUBLE

# ifdef HAVE_LOG2
#  define LOG2(x)     log2(x)
# else /* HAVE_LOG2 */
#  define LOG2(x)     (log(x) / M_LN2)
# endif /* HAVE_LOG2 */
typedef double entropy_t;
# define ENT_FMT "lf"

#else /* ENABLE_DOUBLE */

# ifdef HAVE_LOG2F
#  define LOG2(x)     log2f(x)
# else /* HAVE_LOG2F */
#  define LOG2(x)     (logf(x) / M_LN2)
# endif /* HAVE_LOG2F */
typedef float entropy_t;
# define ENT_FMT "f"

#endif /* ENABLE_DOUBLE */

/**
 ** @def DEFAULT_TIMEOUT
 **   Default timeout value (in second).
 **/
#ifdef ENABLE_TIMEOUT
#define DEFAULT_TIMEOUT 900
#endif /* ENABLE_TIMEOUT */

#define int_ntoa(x)     inet_ntoa(*((struct in_addr *)&x))

/**
 ** @def COMP_ENT
 **   Computes Shannon statistical entropy from character distribution
 **   and data size.
 ** @param stats    A reference to the character distribution array.
 ** @param size     The size of data (sum of all array value).
 ** @param entropy  Variable were computed entropy will be stored.
 **/

#ifdef ENABLE_ALT_ENTROPY_CODE

#define COMP_ENT(stats,size,entropy)                     \
do {                                                     \
  int i;                                                 \
  entropy_t s[256];                                      \
  entropy_t si[256];                                     \
                                                         \
  (entropy) = 0.0f;                                      \
  for (i = 0; i < 256; i++) {                            \
    s[i] = stats[i] / (entropy_t)(size);                 \
    si[i] = 1.0f / s[i];                                 \
  }                                                      \
  for (i = 0; i < 256; i++) {                            \
    if (s[i] > 0) {                                      \
      (entropy) += s[i] * LOG2(si[i]);                   \
    }                                                    \
  }                                                      \
} while (0)

#else /* ENABLE_ALT_ENTROPY_CODE */

#define COMP_ENT(stats,size,entropy)                     \
do {                                                     \
  int i;                                                 \
  entropy_t byteprob;                                    \
                                                         \
  (entropy) = 0.0;                                       \
  for (i = 0; i < 256; i++) {                            \
    if ( (byteprob = (stats[i]) / (entropy_t)(size)) > 0.0) {   \
      (entropy) += byteprob * LOG2(1.0 / byteprob);      \
    }                                                    \
  }                                                      \
} while (0)

#endif /* ENABLE_ALT_ENTROPY_CODE */

#ifdef ENABLE_PANINSKI

#if defined(HAVE_MPFR)
# define PANINSKI_BIAS_CACHED(x) paninski_bias_mpfr_cached(x)
#elif defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL)
# define PANINSKI_BIAS_CACHED(x) paninski_bias_l_cached(x)
#elif defined(HAVE_EXP) && defined(HAVE_LOG)
# define PANINSKI_BIAS_CACHED(x) paninski_bias_cached(x)
#elif defined(HAVE_EXPF) && defined(HAVE_LOGF)
# define PANINSKI_BIAS_CACHED(x) paninski_bias_f_cached(x)
#else
# error "BUG: Error during the selection of paninski estimation functions."
#endif

#define PANINSKI_ADJUST(track_type, entropy, msg_len)    \
do {                                                     \
  if (TRACK_ESTIMATOR(track_type) == TRACK_PANINSKI) {   \
    (entropy) += PANINSKI_BIAS_CACHED(msg_len);          \
  }                                                      \
} while (0)

#else /* ENABLE_PANINSKI */

#define PANINSKI_ADJUST(track_type, entropy, msg_len)

#endif /* ENABLE_PANINSKI */

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE   SYSCONFDIR "net-entropy.conf"
#endif

typedef struct stats_s stats_t;
/**
 ** @struct stats_s
 **   Statistics of a TCP connection.
 **   Contains character distribution and data size to compute entropy.
 **/
/**
 ** @var stats_s::distrib
 **   Contains character distribution.
 **/
/**
 ** @var stats_s::bytes
 **   Size of the data in connection (in bytes).
 **/
/**
 ** @var stats_s::packets
 **   Number of packets exchanged in the connection.
 **/
/**
 ** @var stats_s::alarm
 **   Alarm flag of the connection.  Can be
 **   #ALARM_ENTROPY, #ALARM_RANGE or #ALARM_UPDATE.
 **/
/**
 ** @var stats_s::cur_range
 **   Current range of the connection.
 **/
/**
 ** @var stats_s::cons
 **   Element of the active connection list.
 **/
/**
 ** @var stats_s::tcp
 **   A reference to the libnids TCP stream attached to these statistics.
 **/
/**
 ** @var stats_s::last_act
 **   Date of the last activity of the connection.
 **/
/**
 ** @var stats_s::file
 **   A FILE pointer of the statistic file (if requested).
 **/
struct stats_s {
  unsigned int distrib[256];
  unsigned int bytes;
  unsigned int packets;
  int          alarm;
  int          cur_range;
#ifdef ENABLE_TIMEOUT
  DTAILQ_ENTRY(stats_t) cons;
  struct tcp_stream *tcp;
  time_t       last_act;
#endif /* ENABLE_TIMEOUT */
#ifdef ENABLE_STATISTICS
  FILE        *file;
#endif /* ENABLE_STATISTICS */
};


typedef struct tcp_stream tcp_stream_t;


extern char *config_file_g;
extern int verbose_g;
extern int daemon_g;
extern int learn_mode_g;
extern int delete_old_rules_g;
extern int ports_g;
extern char *runtime_user_g;
extern int priority_g;
extern int memory_limit_g;
extern int max_tracking_size_g;
extern int force_syslog_g;
#ifdef ENABLE_STATISTICS
extern int flush_stats_g;
extern int byte_stat_g;
extern char *statdir_g;
#endif /* ENABLE_STATISTICS */
#ifdef ENABLE_TIMEOUT
extern time_t timeout_g;
extern time_t last_gc_g;
#endif /* ENABLE_TIMEOUT */

const char *
get_dir_string(int type);

#endif /* NET_ENTROPY_H */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
