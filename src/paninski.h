#ifndef PANINSKI_H
#define PANINSKI_H

#ifdef ENABLE_PANINSKI

#if defined(HAVE_MPFR)

/* MPFR default setup for Net-Entropy: precision in bits, and rounding method */
#ifndef MPFR_PRECISION
#define MPFR_PRECISION 512
#endif

#ifndef MPFR_ROUNDING
#define MPFR_ROUNDING  MPFR_RNDN /* use nearest by default */
#endif

entropy_t
paninski_bias_mpfr_cached(int n);
#endif /* defined(HAVE_MPFR) */

#if defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL)
entropy_t
paninski_bias_l_cached(int n);
#endif /* defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL) */

#if defined(HAVE_EXP) && defined(HAVE_LOG)
entropy_t
paninski_bias_cached(int n);
#endif /* defined(HAVE_EXP) && defined(HAVE_LOG) */

#if defined(HAVE_EXPF) && defined(HAVE_LOGF)
entropy_t
paninski_bias_f_cached(int n);
#endif /* defined(HAVE_EXPF) && defined(HAVE_LOGF) */

void
paninski_init_cache(void);

#else /* ENABLE_PANINSKI */

#ifdef HAVE_MPFR
# warning \
  "Enabling MPFR library is relevant only with Paninski's estimator."
#endif /* HAVE_MPFR */

#endif /* ENABLE_PANINSKI */

#endif /* PANINSKI_H */
