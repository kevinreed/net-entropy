/**
 ** @file port_track.c
 ** Main table which describes TCP ports protocol specifications to track
 ** by Net-Entropy.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Mon Oct 19 22:36:07 2009
 ** @date Last update: Sun Nov  1 22:59:07 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "net-entropy.h"

#include "port_track.h"

/** @var ptbl_g
 ** Global port track table.
 **/
port_track_t ptbl_g[MAX_PORTS];

/**
 * Initialise the global port track table ptbl_g.
 **/
void
init_port_track_table(void)
{
  memset(ptbl_g, 0, MAX_PORTS * sizeof(port_track_t));
}


/**
 *  Display all rules before starting (requested with -v verbose switch).
 *
 * @param fp FILE stream to write on.
 **/
void
fprintf_port_track_table(FILE *fp)
{
  int port;
#if 0
  int i;
#endif
  fprintf(fp, "Port track table:\n");
  fprintf(fp, "----------------------------------\n");
  fprintf(fp, " Dir |  Port  |  File\n");
  fprintf(fp, "----------------------------------\n");

  for (port = 0; port < MAX_PORTS; port++) {
    if ( TRACK_DIR( ptbl_g[ port ].type ) ) {
      fprintf(fp, " %3s |  %5i | %5s\n",
              get_dir_string(ptbl_g[ port ].type),
              port,
              ptbl_g[ port ].file);
#if 0
      for (i=0; i < ptbl_g[ port ].range_num; i++)
        printf("%li %li %f %f\n",
               ptbl_g[ port ].ranges[i].start,
               ptbl_g[ port ].ranges[i].end,
               ptbl_g[ port ].ranges[i].ent_min,
               ptbl_g[ port ].ranges[i].ent_max);
#endif
    }
  }
  fprintf(fp, "----------------------------------\n");
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
