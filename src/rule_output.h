#ifndef RULE_OUTPUT_H
#define RULE_OUTPUT_H

void
update_rule(tcp_stream_t *tcp, port_track_t *table);

void
fprintf_rule(FILE *fp, port_track_t *rule);

#endif /* RULE_OUTPUT_H */
