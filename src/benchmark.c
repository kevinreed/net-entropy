/**
 ** @file benchmark.c
 ** Entropy computation benchmarks.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Tue Oct 29 21:32:57 2013
 ** @date Last update: Tue Nov 19 12:47:00 2013
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

/*
 * NOTE: Since Net-Entopy can run on a very wide range of hardware and
 * platforms (from small ambedded ARM routers to big servers),
 * benchmarks will loop until a timer of 3 seconds is expired, or the
 * loop counter overflows.  With this logic, we are almost sure we
 * cover very slow and very fast systems.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
//#include <errno.h>
#include <sys/time.h>
#include <signal.h>
#include <string.h>
#include <limits.h>

#include "net-entropy.h"

#include "benchmark.h"

#define BENCHMARK_TIME 3
#define DATA_SIZE (128 * 1024 * 1024)
#define MAX_VALUE  10000

static double
get_time_as_double(void);
static void
get_human_readable(double value, int power, double *human_value, const char **human_unit);
static void
benchmark_alarm_handler(int sig);
static void
benchmark_histogram(void);
static void
benchmark_entropy(void);
static void
benchmark_packets(void);
static void
init_benchmarks(void);
static void
benchmark_packets_size(const unsigned char *data, const unsigned int size);

static volatile int run_bench_g = 0;


static double
get_time_as_double(void)
{
  double t;
  struct timeval tval;

  gettimeofday(&tval, NULL);

  t = tval.tv_sec + (tval.tv_usec * 1e-6);

  return (t);
}

static void
get_human_readable(double value, int power, double *human_value, const char **human_unit)
{
  const size_t units_sz = 5;
  static const char *units[] = { "", "k", "M", "G", "T" };

  double p;
  int i;
  double hval;

  if (power == 2) {
    p = 1024.0;
  }
  else if (power == 10) {
    p = 1000.0;
  }
  else {
    *human_value = value;
    *human_unit = units[0];

    return ;
  }

  hval = value;
  for (i = 0; i < units_sz && hval > p; i++)
    hval /= p;

  *human_value = hval;
  *human_unit = units[i];
}

static void
benchmark_alarm_handler(int sig)
{
  run_bench_g = 0;
  signal(SIGALRM, benchmark_alarm_handler);
}

static void
benchmark_histogram(void)
{
  unsigned char *data;
  int i;
  unsigned int stats[256];
  double start_time;
  double end_time;
  unsigned long loop;
  double throughput;
  double human_throughput;
  const char *human_unit;
  double human_bit_throughput;
  const char *human_bit_unit;

  printf("histogram: init...\n");

  memset(stats, 0, sizeof (stats));

  data = malloc(DATA_SIZE);
  if (data == NULL) {
    fprintf(stderr, "ERROR\n");
    exit(EXIT_FAILURE);
  }

  for (i = 0; i < DATA_SIZE; i++) {
    data[i] = (unsigned char)(rand() & 0xFF);
  }

  printf("histogram: start\n");
  loop = 0;
  run_bench_g = 1;
  alarm(BENCHMARK_TIME);
  start_time = get_time_as_double();
  i = 0;
  while ( (run_bench_g != 0) && (loop < ULONG_MAX) ) {

    stats[ data[ i++ ] ]++;

    if (i >= DATA_SIZE)
      i = 0;

    loop++;
  }
  end_time = get_time_as_double();

  throughput = loop / (end_time - start_time);
  get_human_readable(throughput, 2, &human_throughput, &human_unit);
  get_human_readable(throughput * 8.0, 10, &human_bit_throughput, &human_bit_unit);

  printf("histogram: %lu bytes in %.3gs: %.2f %sB/s (%.2f %sb/s)\n\n",
         loop, (end_time - start_time),
         human_throughput, human_unit,
         human_bit_throughput, human_bit_unit);

  free(data);
}

static void
benchmark_entropy(void)
{
  unsigned int byte_distrib[256] = { 0, };
  unsigned int sum;
  /* Since we are not using the entropy variable in the loop, we
   * set the variable volatile to prevent compiler optimization. */
  volatile entropy_t entropy;
  int i;
  double start_time;
  double end_time;
  unsigned long loop;
  double human_call;
  const char *human_call_unit;

  printf("entropy: init...\n");

  sum = 0;
  for (i = 0; i < 256; i+=1) {
    unsigned int value;

    value = ( rand() / (RAND_MAX + 1.0) ) * MAX_VALUE;

    sum += value;
    byte_distrib[i] = value;
  }

  entropy = 0.0;

  printf("entropy: start\n");

  start_time = get_time_as_double();
  loop = 0;
  run_bench_g = 1;
  alarm(BENCHMARK_TIME);

  while ( (run_bench_g != 0) && (loop < ULONG_MAX) ) {
    COMP_ENT(byte_distrib, sum, entropy);
    loop++;
  }

  end_time = get_time_as_double();

  get_human_readable(loop / (end_time - start_time), 10,
                     &human_call, &human_call_unit);

  /* Print the entropy, to also make sure there is no code removal
   * optimization from the compiler */
  printf("entropy: info: measured entropy %.3f bits/Byte\n", entropy);
  printf("entropy: %lu computation calls in %.3gs: %.2f %scall/s\n\n",
         loop, (end_time - start_time),
         human_call, human_call_unit);
}

static void
benchmark_packets(void)
{
  unsigned char data[9000];
  int i;

  printf("packets: init...\n");

  for (i = 0; i < sizeof (data); i++) {
    data[i] = (unsigned char)(rand() & 0xFF);
  }

  printf("packets: start\n");

  benchmark_packets_size(data, 64);
  benchmark_packets_size(data, 128);
  benchmark_packets_size(data, 512);
  benchmark_packets_size(data, 1024);
  benchmark_packets_size(data, 1500);
  benchmark_packets_size(data, 9000);

  printf("\n");
}

static void
benchmark_packets_size(const unsigned char *data, const unsigned int size)
{
  int i;
  unsigned int stats[256];
  /* Since we are not using the entropy variable in the loop, we
   * set the variable volatile to prevent compiler optimization. */
  volatile entropy_t entropy;
  unsigned long sum;
  double start_time;
  double end_time;
  unsigned long loop;
  double byte_throughput;
  double human_byte_throughput;
  const char *human_byte_unit;
  double human_bit_throughput;
  const char *human_bit_unit;
  double human_pps;
  const char *human_pps_unit;

  memset(stats, 0, sizeof (stats));

  sum = 0;
  entropy = 0.0;
  run_bench_g = 1;
  loop = 0;
  alarm(BENCHMARK_TIME);
  start_time = get_time_as_double();

  while ( (run_bench_g != 0) && (loop < ULONG_MAX)) {

    for (i = 0; i < size; i++)
      stats[ data[ i ] ]++;

    sum += size;

    COMP_ENT(stats, sum, entropy);

    loop++;
  }

  end_time = get_time_as_double();

  byte_throughput = sum / (end_time - start_time);

  get_human_readable(byte_throughput, 2,
                     &human_byte_throughput, &human_byte_unit);
  get_human_readable(byte_throughput * 8.0, 10,
                     &human_bit_throughput, &human_bit_unit);

  get_human_readable(loop / (end_time - start_time), 10,
                     &human_pps, &human_pps_unit);

  /* Print the entropy, to also make sure there is no code removal
   * optimization from the compiler */
  printf("packets: info: measured entropy %.3f bits/Byte\n", entropy);
  printf("packets: size %u: "
         "%lu pkts in %.3gs: %.2f %spkt/s, "
         "%.2f %sB/s (%.2f %sb/s)\n",
         size,
         loop, (end_time - start_time),
         human_pps, human_pps_unit,
         human_byte_throughput, human_byte_unit,
         human_bit_throughput, human_bit_unit);
}

static void
init_benchmarks(void)
{
  struct timeval t;

  gettimeofday(&t, NULL);

  unsigned int seed = (unsigned int)(t.tv_sec ^ t.tv_usec);

  printf("Using random seed 0x%08x\n\n", seed);

  /* For simple benchmarks, we don't really need good randomness.
   * The standard libc rand() will be enough. */
  srand( seed );
}

void
run_benckmarks(void)
{
  signal(SIGALRM, benchmark_alarm_handler);

  printf("\n*** NOTE: ***\n");
  printf("These benchmarks only measure pure CPU speed of Net-Entropy\n");
  printf("histogram and entropy computation code.\n");
  printf("In real deployments, you'll have to take other system components\n");
  printf("into account; mainly: hardware, kernel, NIC driver, libpcap, libnids...\n");
  printf("For real world statistics, use the performance counters feature of Net-Entropy.\n\n");

  init_benchmarks();

  benchmark_histogram();
  benchmark_entropy();
  benchmark_packets();
}

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
