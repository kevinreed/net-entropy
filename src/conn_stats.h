#ifndef CONN_STATS_H
#define CONN_STATS_H

#ifdef ENABLE_STATISTICS

void
fopen_statfiles(struct tcp_stream *a_tcp, stats_t *stats);

int
check_stat_dir(const char *dir);

#endif /* ENABLE_STATISTICS */

#endif /* CONN_STATS_H */
