#ifndef PORT_TRACK_H
#define PORT_TRACK_H

#include "int_range.h"

/**
 ** @struct range_s
 **   A range of valid entropy values for a size range.
 **/
/**
 ** @var range_s::start
 **   Minimal size of the range.
 **/
/**
 ** @var range_s::end
 **   Maximal size of the range.
 **/
/**
 ** @var range_s::ent_min
 **   Minimal entropy value authorized in the range.
 **/
/**
 ** @var range_s::ent_max
 **   Maximal entropy value authorized in the range.
 **/
typedef struct range_s range_t;
struct range_s {
  long start;
  long end;
  entropy_t ent_min;
  entropy_t ent_max;
};


/**
 ** @struct port_track_s
 **   Element of the global rule table.
 **/
/**
 ** @var port_track_s::type
 **    Defines the type of analysis for this rule.
 **/
/**
 ** @var port_track_s::ranges
 **    Array of valid ranges.
 **/
/**
 ** @var port_track_s::range_num
 **    Number of elements in the range array.
 **/
/**
 ** @var port_track_s::file
 **    Rule file name.
 **/
/**
 ** @var port_track_s::port_def
 **    Port definition as in the protocol specification file.
 **    Since ports may be shared over a same protocol specification,
 **    we now need to remember the original definition in case we want
 **    to update the specification file in the automatic learning mode.
 **    This value may be defined to #ALL_PORTS .
 **/
typedef struct port_track_s port_track_t;
struct port_track_s {
  int type;
  range_t *ranges;
  int range_num;
  char *file;
  int_range_t *port_def;
  size_t port_def_size;
};


extern port_track_t ptbl_g[MAX_PORTS];


void
init_port_track_table(void);

void
fprintf_port_track_table(FILE *fp);


#endif /* PORT_TRACK_H */
