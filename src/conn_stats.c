/**
 ** @file net-entropy.c
 ** Net-entropy: TCP Connection entropy checker.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Wed Mar 17 02:13:15 2004
 ** @date Last update: Thu May 27 13:43:39 2010
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#ifdef ENABLE_STATISTICS

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <nids.h>
#include <limits.h>

#include "net-entropy.h"

#include "conn_stats.h"
#include "log.h"
#include "port_track.h"

/**
 *  Check if a statistics directory exists.
 *
 * @param dir   Path to check.
 * @return 1 if exists, 0 otherwise. 
 **/
int
check_stat_dir(const char *dir)
{
  struct stat dir_stat;
  int ret;

  ret = stat(dir, &dir_stat);

  if (ret == 0 && S_ISDIR(dir_stat.st_mode))
    return (1);

  log_error("stat(): %s\n", strerror(errno));

  return (0);
}


/**
 * Create and open a statistic file.
 *
 * @param a_tcp The current libnids TCP stream.
 * @param stats Statistics of the current stream.
 **/
void
fopen_statfiles(struct tcp_stream *a_tcp, stats_t *stats)
{
  char src_ip[16];
  char dst_ip[16];
  char filename[PATH_MAX];
  struct timeval tv;
  FILE *fp;

  snprintf(src_ip, sizeof (src_ip), int_ntoa(a_tcp->addr.saddr));
  snprintf(dst_ip, sizeof (dst_ip), int_ntoa(a_tcp->addr.daddr));

  gettimeofday(&tv, NULL);

  snprintf(filename, sizeof (filename), "%s/%li.%06li.dat",
           statdir_g,
           (long int)tv.tv_sec, (long int)tv.tv_usec);

  fp = fopen(filename, "w");
  if (fp == NULL) {
    log_error("fopen(\"%s\", \"w\"): error %i: %s\n",
              filename,
              errno, strerror(errno));
    return ;
  }

  fprintf(fp,
          "# Net-Entropy connection data\n"
          "# Connection info: "
          "(src_ip:src_port direction dst_ip:dst_port):\n");
  fprintf(fp, "# connection-info: %s:%i %s %s:%i\n#\n",
          src_ip, a_tcp->addr.source,
          get_dir_string(ptbl_g[ a_tcp->addr.dest ].type),
          dst_ip, a_tcp->addr.dest);
  fprintf(fp,
          "# Field description:\n"
          "# 1st field: offset in data stream\n"
          "# 2nd field: entropy value of data "
            "from the begin to the current offset\n"
          "# 3rd field: date, as returned by gettimeofday()\n"
          "# 4th field: direction: "
            "0 means client to server, 1 means server to client\n"
          "# 5th field: tell if the offset is the end of a packet "
            "(0=NO, 1=YES)\n");

  stats->file = fp;
}


#endif /* ENABLE_STATISTICS */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
