#ifndef PANINSKI_PRIV_H
#define PANINSKI_PRIV_H

#include <sys/types.h>

#define PANINSKI_CACHE_SIZE 131072
#ifndef PANINSKI_CACHE_FILE
#define PANINSKI_CACHE_FILE LOCALSTATEDIR "/cache/paninski.cache"
#endif /* PANINSKI_CACHE_FILE */

#if defined(HAVE_MPFR)
# define PANINSKI_BIAS(x) paninski_bias_mpfr_series(x)
#elif defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL)
# define PANINSKI_BIAS(x) paninski_bias_l((long double)(x) / 256.0)
#elif defined(HAVE_EXP) && defined(HAVE_LOG)
# define PANINSKI_BIAS(x) paninski_bias((double)(x) / 256.0)
#elif defined(HAVE_EXPF) && defined(HAVE_LOGF)
# define PANINSKI_BIAS(x) paninski_bias_f((float)(x) / 256.0)
#else
# error "BUG: Error during the selection of paninski estimation functions."
#endif

#if defined(HAVE_MPFR)
static entropy_t
paninski_bias_mpfr_series(unsigned long n);
#endif /* defined(HAVE_MPFR) */

#if defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL)
static long double
paninski_bias_l_approx(long double c);
static long double
paninski_bias_l_series(long double c);
static long double
paninski_bias_l(long double c);
#endif /* defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL) */

#if defined(HAVE_EXP) && defined(HAVE_LOG)
static double
paninski_bias_approx(double c);
static double
paninski_bias_series(double c);
static double
paninski_bias(double c);
#endif /* defined(HAVE_EXP) && defined(HAVE_LOG) */

#if defined(HAVE_EXPF) && defined(HAVE_LOGF)
static float
paninski_bias_f_approx(float c);
static float
paninski_bias_f_series(float c);
static float
paninski_bias_f(float c);
#endif /* defined(HAVE_EXPF) && defined(HAVE_LOGF) */

static void
paninski_cache_save(void);
static void
paninski_compute_cache(void);

#define PANINSKI_CACHE_MAGIC "PAN"
#define PANINSKI_CACHE_VERSION 1

typedef struct paninski_cache_hdr_s paninski_cache_hdr_t;
struct paninski_cache_hdr_s
{
  char magic[4]; /* should be "PAN" 0 terminated string */
  int version;
  size_t sizeof_float;
  size_t cache_size;
};

#endif /* PANINSKI_PRIV_H */
