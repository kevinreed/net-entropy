#ifndef CFG_FILE_H
#define CFG_FILE_H

#include "port_track.h"

void
read_config_file(const char *file);

void
read_spec_file(port_track_t *tbl, const char *file);

#if defined(HAVE_GLOB_H) && defined(HAVE_GLOB)
void
read_spec_files(const char *pattern);
#endif

#endif /* CFG_FILE_H */
