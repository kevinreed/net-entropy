/**
 ** @file nids_callback.c
 ** libnids TCP callback.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Nov  1 22:35:08 2009
 ** @date Last update: Fri Jul  2 23:25:18 2010
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <nids.h>

#include "net-entropy.h"

#include "port_track.h"
#include "paninski.h"
#include "log_event.h"
#include "dtailq.h"
#include "conn_stats.h"
#include "conn_timeout.h"
#include "rule_output.h"
#include "nids_callback.h"
#include "perf_stats.h"

static void
learn_entropy(tcp_stream_t *a_tcp, stats_t *stats, int count_new);
static void
upd_range_and_check_ent(tcp_stream_t *a_tcp, stats_t *stats, int count_new);


/**
 *  The libnids TCP callback.  This is the main loop of Net-Entropy.
 *
 * @param a_tcp  The current libnids TCP stream being processed.
 * @param user   Arbitraty user data attached to the TCP stream.
 *               This is statistics in this case.
 **/
void
tcp_callback(struct tcp_stream *a_tcp, void **user)
{
  int size;
  stats_t *stats;
  unsigned int *distrib;
  unsigned char *data;
#ifdef ENABLE_STATISTICS
  entropy_t e;
#endif
#if defined(ENABLE_TIMEOUT) || defined(ENABLE_PERF_STATS)
  time_t curtime;
#endif

  if ((a_tcp->client.collect == 0 && a_tcp->server.collect == 0)
      && a_tcp->nids_state != NIDS_JUST_EST) {
    return ;
  }

#if defined(ENABLE_TIMEOUT) || defined(ENABLE_PERF_STATS)
  curtime = time(NULL);
#endif

  perf_stats_tcp_pkt();

  perf_stats(curtime);

#ifdef ENABLE_TIMEOUT
  if (curtime - last_gc_g > timeout_g) {
    if (destroy_inactive_connections(curtime, a_tcp)) {
      return ;
    }
  }
#endif /* ENABLE_TIMEOUT */

  if (a_tcp->nids_state == NIDS_DATA) {

    stats = (stats_t *)*user;

    perf_stats_tcp_data_pkt();

#ifdef ENABLE_TIMEOUT
      stats->last_act = curtime;
#endif /* ENABLE_TIMEOUT */

    /* server side (client to server) */
    size = a_tcp->server.count_new;
    if (size > 0) {
      stats->packets++;
      stats->bytes += size;
      distrib = stats->distrib;
      data = (unsigned char *) a_tcp->server.data;
      while (size--) {
        distrib[ *data++ ]++;

#ifdef ENABLE_STATISTICS
        if (statdir_g && (size == 0 || byte_stat_g)) {
          struct timeval tv;
          gettimeofday(&tv, NULL);
          if ( TRACK_IS_CUMUL( ptbl_g[ a_tcp->addr.dest ].type ) ) {
            COMP_ENT(distrib, stats->bytes - size, e);
            PANINSKI_ADJUST(ptbl_g[ a_tcp->addr.dest ].type,
                            e,
                            stats->bytes - size);
          }
          else {
            COMP_ENT(distrib, a_tcp->server.count_new - size, e);
            PANINSKI_ADJUST(ptbl_g[ a_tcp->addr.dest ].type,
                            e,
                            a_tcp->server.count_new - size);
          }
          if (stats->file) {
            fprintf(stats->file, "%u\t%10.8f\t%li.%06li\t0\t%i\n",
                    stats->bytes - size, e,
                    (long int)tv.tv_sec, (long int)tv.tv_usec,
                    size ? 0 : 1);
            if (flush_stats_g)
              fflush(stats->file);
          }
        }
#endif /* ENABLE_STATISTICS */
      }

      if (learn_mode_g)
        learn_entropy(a_tcp, stats, a_tcp->server.count_new);
      else
        upd_range_and_check_ent(a_tcp, stats, a_tcp->server.count_new);

      /* if entropy is per-packet, clear statistics */
      if ( ! TRACK_IS_CUMUL( ptbl_g[ a_tcp->addr.dest ].type ) ) {
        memset(stats->distrib, 0, sizeof (unsigned int) * 256);
      }
    }

    /* client side (server to client) */
    size = a_tcp->client.count_new;
    if (size > 0) {
      stats->packets++;
      stats->bytes += size;
      distrib = stats->distrib;
      data = (unsigned char *) a_tcp->client.data;
      while (size--) {
        distrib[ *data++ ]++;

#ifdef ENABLE_STATISTICS
        if (statdir_g && (size == 0 || byte_stat_g)) {
          struct timeval tv;
          gettimeofday(&tv, NULL);
          if ( TRACK_IS_CUMUL( ptbl_g[ a_tcp->addr.dest ].type ) ) {
            COMP_ENT(distrib, stats->bytes - size, e);
            PANINSKI_ADJUST(ptbl_g[ a_tcp->addr.dest ].type,
                            e,
                            stats->bytes - size);
          }
          else {
            COMP_ENT(distrib, a_tcp->client.count_new - size, e);
            PANINSKI_ADJUST(ptbl_g[ a_tcp->addr.dest ].type,
                            e,
                            a_tcp->client.count_new - size);
          }
          if (stats->file) {
            fprintf(stats->file, "%u\t%10.8f\t%li.%06li\t1\t%i\n",
                    stats->bytes - size, e,
                    (long int)tv.tv_sec, (long int)tv.tv_usec,
                    size ? 0 : 1);
            if (flush_stats_g)
              fflush(stats->file);
          }
        }
#endif /* ENABLE_STATISTICS */
      }

      if (learn_mode_g)
        learn_entropy(a_tcp, stats, a_tcp->client.count_new);
      else
        upd_range_and_check_ent(a_tcp, stats, a_tcp->client.count_new);

      /* if entropy is per-packet, clear statistics */
      if ( ! TRACK_IS_CUMUL( ptbl_g[ a_tcp->addr.dest ].type ) ) {
        memset(stats->distrib, 0, sizeof (unsigned int) * 256);
      }
    }

    if (stats->bytes > max_tracking_size_g) {
      a_tcp->client.collect = 0;
      a_tcp->server.collect = 0;
      if (stats->alarm) {
	log_max_data(a_tcp, stats);
      }
      perf_stats_tcp_size_limit();
#ifdef ENABLE_TIMEOUT
      DTAILQ_REMOVE(&con_list_g, stats, cons);
#endif /* ENABLE_TIMEOUT */
#ifdef ENABLE_STATISTICS
      if (statdir_g && stats->file) {
	if (learn_mode_g && (stats->alarm & ALARM_UPDATE))
	  fprintf(stats->file,
		  "# Info: This connection updated rule\n");
	fprintf(stats->file,
		"# Stop tracking data=%i > maxdata=%i, packets=%i\n",
		stats->bytes,
		max_tracking_size_g,
		stats->packets);
	fclose(stats->file);
      }
#endif /* ENABLE_STATISTICS */
      if (learn_mode_g && (stats->alarm & ALARM_UPDATE))
	update_rule(a_tcp, ptbl_g);

      free(stats);
      stats = NULL;
      *user = NULL;
    }
  }
  else if (a_tcp->nids_state == NIDS_JUST_EST) {
    int type;

    perf_stats_inc_tcp_conn();
    type = ptbl_g[ a_tcp->addr.dest ].type;
    if ( TRACK_DIR(type) ) {
      stats = malloc(sizeof (stats_t));
      *user = stats;
      memset(stats, 0, sizeof (stats_t));
      if ( TRACK_DIR(type) & TRACK_SRV2CLI) {
        a_tcp->client.collect++;
      }
      if ( TRACK_DIR(type) & TRACK_CLI2SRV) {
        a_tcp->server.collect++;
      }

#ifdef ENABLE_TIMEOUT
      DTAILQ_INSERT_TAIL(&con_list_g, stats, cons);
      stats->last_act = curtime;
      stats->tcp = a_tcp;
#endif /* ENABLE_TIMEOUT */

#ifdef ENABLE_STATISTICS
      if (statdir_g) {
        fopen_statfiles(a_tcp, stats);
      }
#endif /* ENABLE_STATISTICS */
    }
  }
  else if ( (a_tcp->nids_state == NIDS_CLOSE) ||
            (a_tcp->nids_state == NIDS_RESET) ||
            (a_tcp->nids_state == NIDS_EXITING)) {
    stats = (stats_t *) *user;
    if (stats) {
      if (stats->alarm) {
        log_end_of_connection(a_tcp, stats);
      }
      perf_stats_tcp_closed();
#ifdef ENABLE_TIMEOUT
      DTAILQ_REMOVE(&con_list_g, stats, cons);
#endif /* ENABLE_TIMEOUT */
#ifdef ENABLE_STATISTICS
      if (statdir_g && stats->file) {
        if (learn_mode_g && (stats->alarm & ALARM_UPDATE))
          fprintf(stats->file,
                  "# Info: This connection updated rule\n");
        if (a_tcp->nids_state == NIDS_EXITING)
          fprintf(stats->file,
                  "# Stop tracking due to program termination request.\n");
        else
          fprintf(stats->file, "# End of connection\n");
        fclose(stats->file);
      }
#endif /* ENABLE_STATISTICS */
      if (learn_mode_g && (stats->alarm & ALARM_UPDATE))
        update_rule(a_tcp, ptbl_g);

      free(stats);
      stats = NULL;
      *user = NULL;
    }
  }
}

/**
 * Size and entropy checking function.
 * This is the main analysis code.  Checks that the connection is in defined
 * ranges, and that the entropy is in specified bounds.
 * Cases are:
 * - case 1: where are before the first defined range -> alarm.
 * - case 2: we are in a range:
 *   - case 2.1: entropy is OK for this range. -> fall alarm.
 *   - case 2.2: entropy is NOT OK. -> raise alarm.
 *   .
 * - case 3: we are between two ranges -> raise alarm.
 * - case 4: we are after the last range
 *   - case 4.1: end of the last range < max-track -> alarm.
 *   - case 4.2: end of the last range >= max-track
 *     - case 4.2.1: entropy is OK in respect of the last range.
 *     - case 4.2.2: entropy is NOT OK -> alarm.
 *
 * @param a_tcp      The current libnids TCP stream being processed.
 * @param stats      The statistics of the current tcp stream.
 * @param count_new  The size of the last captured packet.
 **/
static void
upd_range_and_check_ent(tcp_stream_t *a_tcp, stats_t *stats, int count_new)
{
  unsigned int *distrib;
  entropy_t e;
  range_t *ranges;
  int range_num;
  long offset;
  int size;
  port_track_t *pt;

  pt = &ptbl_g[ a_tcp->addr.dest ];
  ranges = pt->ranges;
  range_num = pt->range_num - 1;

  if (TRACK_TYPE(pt->type) == TRACK_PACKETS)
    offset = stats->packets;
  else
    offset = stats->bytes;

  if (TRACK_IS_CUMUL(pt->type) )
    size = stats->bytes;
  else
    size = count_new;

  if (stats->cur_range == 0 &&
      offset < ranges[0].start &&
      !(stats->alarm & ALARM_RANGE)) {
    /* case 1 */
    log_out_of_range_alarm(a_tcp, stats);
    stats->alarm |= ALARM_RANGE;
  }
  else {
    while (stats->cur_range < range_num &&
           offset > ranges[stats->cur_range].end)
      stats->cur_range++;

    if (stats->cur_range == range_num &&
        offset > ranges[stats->cur_range].end) {
      /* case 4 */
      if (ranges[stats->cur_range].end < max_tracking_size_g) {
        /* case 4.1 */
        if (!(stats->alarm & ALARM_RANGE)) {
          log_out_of_range_alarm(a_tcp, stats);
          stats->alarm |= ALARM_RANGE;
        }
      }
      else {
        /* case 4.2 */
        distrib = stats->distrib;
        COMP_ENT(distrib, size, e);
        PANINSKI_ADJUST(pt->type, e, size);
        if ((e < ranges[stats->cur_range].ent_min ||
             e > ranges[stats->cur_range].ent_max) &&
            !(stats->alarm & ALARM_ENTROPY)) {
          /* case 4.2.2 */
          log_rising_alarm(a_tcp, stats, e);
          stats->alarm |= ALARM_ENTROPY;
        }
        else if (((e >= ranges[stats->cur_range].ent_min &&
                   e <= ranges[stats->cur_range].ent_max) &&
                  (stats->alarm & ALARM_ENTROPY))) {
          /* case 4.2.1 */
          log_falling_alarm(a_tcp, stats, e);
          stats->alarm &= ~ALARM_ENTROPY;
        }
      }
    }
    else if (offset <= ranges[stats->cur_range].end &&
             offset < ranges[stats->cur_range].start &&
             !(stats->alarm & ALARM_RANGE)) {
      /* case 3 */
      log_out_of_range_alarm(a_tcp, stats);
      stats->alarm |= ALARM_RANGE;
    }
    else if (offset <= ranges[stats->cur_range].end &&
             offset >= ranges[stats->cur_range].start) {
      /* case 2*/
      if (stats->alarm & ALARM_RANGE) {
        log_reenter_range(a_tcp, stats);
        stats->alarm &= ~ALARM_RANGE;
      }

      distrib = stats->distrib;
      COMP_ENT(distrib, size, e);
      PANINSKI_ADJUST(pt->type, e, size);
      if ((e < ranges[stats->cur_range].ent_min ||
           e > ranges[stats->cur_range].ent_max) &&
          !(stats->alarm & ALARM_ENTROPY)) {
        /* case 2.2 */
        log_rising_alarm(a_tcp, stats, e);
        stats->alarm |= ALARM_ENTROPY;
      }
      else if ((e >= ranges[stats->cur_range].ent_min &&
                e <= ranges[stats->cur_range].ent_max) &&
               (stats->alarm & ALARM_ENTROPY)) {
        /* case 2.1 */
        log_falling_alarm(a_tcp, stats, e);
        stats->alarm &= ~ALARM_ENTROPY;
      }
    }
  }
}



/**
 * Size and entropy learning function.
 * This function is used instead of the checking function
 * upd_range_and_check_ent() to learn a protocol.
 * Cases are:
 * - case 1: where are before the first defined range -> alarm.
 * - case 2: we are in a range:
 *   - case 2.1: entropy is OK for this range -> nothing.
 *   - case 2.2: entropy is NOT OK -> update value.
 *   .
 * - case 3: we are between two ranges -> raise range alarm.
 * - case 4: we are after the last range
 *   - case 4.1: end of the last range < max-track -> update.
 *   - case 4.2: end of the last range >= max-track
 *     - case 4.2.1: entropy is OK in respect of the last range -> nothing.
 *     - case 4.2.2: entropy is NOT OK -> update value.
 *
 * @param a_tcp      The current libnids TCP stream being processed.
 * @param stats      The statistics of the current tcp stream.
 * @param count_new  The size of the last captured packet.
 **/
static void
learn_entropy(tcp_stream_t *a_tcp, stats_t *stats, int count_new)
{
  unsigned int *distrib;
  entropy_t e;
  range_t *ranges;
  int range_num;
  long offset;
  int size;
  port_track_t *pt;

  pt = &ptbl_g[ a_tcp->addr.dest ];
  ranges = pt->ranges;
  range_num = pt->range_num - 1;

  if (TRACK_TYPE(pt->type) == TRACK_PACKETS)
    offset = stats->packets;
  else
    offset = stats->bytes;

  if (TRACK_IS_CUMUL(pt->type) )
    size = stats->bytes;
  else
    size = count_new;

  if (stats->cur_range == 0 &&
      offset < ranges[0].start &&
      !(stats->alarm & ALARM_RANGE)) {
    /* case 1 */
    log_out_of_range_alarm(a_tcp, stats);
    stats->alarm |= ALARM_RANGE;
  }
  else {
    while (stats->cur_range < range_num &&
           offset > ranges[stats->cur_range].end)
      stats->cur_range++;

    if (stats->cur_range == range_num &&
        offset > ranges[stats->cur_range].end) {
      /* case 4 */
      if (ranges[stats->cur_range].end < max_tracking_size_g) {
        /* case 4.1 */
        if (!(stats->alarm & ALARM_RANGE)) {
          log_out_of_range_alarm(a_tcp, stats);
          stats->alarm |= ALARM_RANGE;
        }
      }
      else {
        /* case 4.2 */
        distrib = stats->distrib;
        COMP_ENT(distrib, size, e);
        PANINSKI_ADJUST(pt->type, e, size);

        if (isnan(ranges[stats->cur_range].ent_min) &&
            isnan(ranges[stats->cur_range].ent_max)) {
          log_new_value(a_tcp, stats, e);
          ranges[stats->cur_range].ent_min = e;
          ranges[stats->cur_range].ent_max = e;
          stats->alarm |= ALARM_UPDATE;
        }

        if (e < ranges[stats->cur_range].ent_min ||
            isnan(ranges[stats->cur_range].ent_min)) {
          log_update_min_value(a_tcp, stats, e);
          ranges[stats->cur_range].ent_min = e;
          stats->alarm |= ALARM_UPDATE;
        }

        if (e > ranges[stats->cur_range].ent_max ||
            isnan(ranges[stats->cur_range].ent_max)) {
          log_update_max_value(a_tcp, stats, e);
          ranges[stats->cur_range].ent_max = e;
          stats->alarm |= ALARM_UPDATE;
        }
      }
    }
    else if (offset <= ranges[stats->cur_range].end &&
             offset < ranges[stats->cur_range].start &&
             !(stats->alarm & ALARM_RANGE)) {
      /* case 3 */
      log_out_of_range_alarm(a_tcp, stats);
      stats->alarm |= ALARM_RANGE;
    }
    else if (offset <= ranges[stats->cur_range].end &&
             offset >= ranges[stats->cur_range].start) {
      /* case 2*/
      if (stats->alarm & ALARM_RANGE) {
        log_reenter_range(a_tcp, stats);
        stats->alarm &= ~ALARM_RANGE;
      }

      distrib = stats->distrib;
      COMP_ENT(distrib, size, e);
      PANINSKI_ADJUST(pt->type, e, size);

      if (isnan(ranges[stats->cur_range].ent_min) &&
          isnan(ranges[stats->cur_range].ent_max)) {
        log_new_value(a_tcp, stats, e);
        ranges[stats->cur_range].ent_min = e;
        ranges[stats->cur_range].ent_max = e;
        stats->alarm |= ALARM_UPDATE;
      }

      if (e < ranges[stats->cur_range].ent_min ||
          isnan(ranges[stats->cur_range].ent_min)) {
        log_update_min_value(a_tcp, stats, e);
        ranges[stats->cur_range].ent_min = e;
        stats->alarm |= ALARM_UPDATE;
      }

      if (e > ranges[stats->cur_range].ent_max ||
          isnan(ranges[stats->cur_range].ent_max)) {
        log_update_max_value(a_tcp, stats, e);
        ranges[stats->cur_range].ent_max = e;
        stats->alarm |= ALARM_UPDATE;
      }
    }
  }
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
