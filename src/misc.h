#ifndef MISC_H
#define MISC_H

void
daemonize(const char *stdout_file, const char *stderr_file);

void
change_run_id(char *username);

void
set_memory_limit(int limit);

void
set_process_priority(int prio);

const char *
get_dir_string(int type);

#endif /* MISC_H */
