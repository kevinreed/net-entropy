/**
 ** @file cfg_cmdline.c
 ** Command line configuration functions.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Wed Mar 17 02:13:15 2004
 ** @date Last update: Thu Dec  5 00:16:21 2013
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/utsname.h>
#include <nids.h>
#include <errno.h>
#ifdef HAVE_MPFR
#include <mpfr.h>
#endif

#include "net-entropy.h"

#include "cfg_cmdline.h"
#include "port_track.h"
#include "cfg_file.h"
#ifdef ENABLE_STATISTICS
#include "conn_stats.h"
#endif /* ENABLE_STATISTICS */
#include "log.h"
#include "benchmark.h"
#include "paninski.h"

static void
quiet_nids_syslog(int type, int err, void *iph, void *data);

static void
net_entropy_version(FILE *fp);


#ifdef ENABLE_TIMEOUT
# define OPTS_TIMEOUT "T:"
#else /* ENABLE_TIMEOUT */
# define OPTS_TIMEOUT
#endif /* ENABLE_TIMEOUT */

#ifdef ENABLE_STATISTICS
# define OPTS_STATISTICS "bFs:"
#else /* ENABLE_STATISTICS */
# define OPTS_STATISTICS
#endif /* ENABLE_STATISTICS */

#define OPTS_NET_ENTROPY                              \
  "Bc:df:hi:lm:nP:qr:St:u:vV"                        \
  OPTS_TIMEOUT                                        \
  OPTS_STATISTICS

/**
 * Command line parser.
 *
 * @param argc  Argument counter.
 * @param argv  Argument vector.
 **/
void
parse_cmdline(int argc, char *argv[])
{
  char opt;

  while ((opt = getopt(argc, argv, OPTS_NET_ENTROPY)) != -1) {

    switch (opt) {

    case 'h':
      net_ent_usage();
      exit(EXIT_SUCCESS);
      break;

    case 'V':
      net_entropy_version(stdout);
      exit(EXIT_SUCCESS);
      break;

    case 'i':
      nids_params.device = strdup(optarg);
      break;

    case 'r':
      nids_params.filename = strdup(optarg);
      daemon_g = DAEMON_NO;
      break;

    case 'S':
      force_syslog_g = 1;
      break ;

    case 'f':
      nids_params.pcap_filter = strdup(optarg);
      break;

    case 'P':
      ports_g++;
      read_spec_file(ptbl_g, optarg);
      break;

    case 'v':
      verbose_g = 1;
      break;

    case 'u':
      runtime_user_g = strdup(optarg);
      break;

    case 'n':
      daemon_g = DAEMON_NO;
      break;

    case 'm':
      memory_limit_g = 1024 * atoi(optarg);
      break;

    case 't':
      max_tracking_size_g = atoi(optarg);
      break;

    case 'l':
      learn_mode_g = 1;
      break;

    case 'd':
      delete_old_rules_g = 1;
      break;

    case 'q':
      nids_params.syslog = quiet_nids_syslog;
      break ;

#ifdef ENABLE_TIMEOUT
    case 'T':
      timeout_g = atoi(optarg);
      break;
#endif /* ENABLE_TIMEOUT */

#ifdef ENABLE_STATISTICS
    case 's':
      statdir_g = strdup(optarg);
      if ( !check_stat_dir(statdir_g) ) {
        log_error("ERROR: statistic directory doesn't exist.\n");
        exit(EXIT_FAILURE);
      }
      break;

    case 'F':
      flush_stats_g = 1;
      break;

    case 'b':
      byte_stat_g = 1;
      break;
#endif /* ENABLE_STATISTICS */

    case 'c':
      config_file_g = strdup(optarg);
      break;

    case 'B':
      run_benckmarks();
      exit(0);

    default:
      net_ent_usage();
      exit(EXIT_FAILURE);
    }
  }
}



/**
 * Display a very quick help.
 **/
void
net_ent_usage(void)
{
  fprintf(stderr, "\nnet-entropy usage:\n\n");
  fprintf(stderr, "net-entropy <options>\n");
  fprintf(stderr, "  -r <filename>               Run offline on pcap file\n");
  fprintf(stderr, "  -i <interface>              Set capture interface\n");
  fprintf(stderr, "  -f <pcap-filter>            Set capture filter\n");
  fprintf(stderr, "  -u <user>                   Set runtime user\n");
  fprintf(stderr, "  -m <size>                   Set data mem rlimit (KB)\n");
  fprintf(stderr, "  -t <size>                   Set max tracking size (default %i Bytes)\n",
          max_tracking_size_g);
#ifdef ENABLE_TIMEOUT
  fprintf(stderr, "  -T <sec>                    Set the timeout (default %li seconds)\n",
          (long int)timeout_g);
#endif /* ENABLE_TIMEOUT */
  fprintf(stderr, "  -n                          No daemon (run in foreground,\n");
  fprintf(stderr, "                                and output all messages on stderr)\n");
  fprintf(stderr, "  -S                          Force message logging to syslog()\n");
  fprintf(stderr, "                                (only useful with -n)\n");
#ifdef ENABLE_STATISTICS
  fprintf(stderr, "  -s <stat-dir>               Set statistic files directory\n");
  fprintf(stderr, "  -F                          Flush each statistic line\n");
  fprintf(stderr, "  -b                          Log entropy for each byte\n");
#endif /* ENABLE_STATISTICS */
  fprintf(stderr, "  -v                          Verbose mode\n");
  fprintf(stderr, "  -V                          Show version and build info\n");
  fprintf(stderr, "  -h                          This help\n");
  fprintf(stderr, "  -l                          Learn mode\n");
  fprintf(stderr, "  -d                          Delete old rules in learn mode\n");
  fprintf(stderr, "  -P <rule>                   Port spec file\n");
  fprintf(stderr, "  -c <config.file>            Set configuration file to use\n");
  fprintf(stderr, "  -q                          Don't log libnids messages\n");
  fprintf(stderr, "  -B                          Run entropy computation benchmarks\n");
  fprintf(stderr, "\n  Exemple:\n");
  fprintf(stderr, "    net-entropy -i eth0 -f \"tcp and port 22\" -p 22.rule\n\n");
}

static void
fprintf_sysinfo(FILE *fp)
{
  struct utsname un;
  int ret;

  ret = uname(&un);
  if (ret != 0) {
    log_error("fprintf_sysinfo(): uname(): %s.\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  fprintf(fp, "System name:                        %s\n", un.sysname);
  fprintf(fp, "System release:                     %s\n", un.release);
  fprintf(fp, "System version:                     %s\n", un.version);
  fprintf(fp, "System machine:                     %s\n", un.machine);
}

/**
 * Display the version and build configuration of Net-Entropy.
 **/
static void
net_entropy_version(FILE *fp)
{
#ifdef SVN_REV
  fprintf(fp, "\nNet-Entropy %s SVN rev %s (build %s %s)\n\n",
          VERSION, SVN_REV, __DATE__, __TIME__);
#else
  fprintf(fp, "\nNet-Entropy %s (build %s %s)\n\n",
          VERSION, __DATE__, __TIME__);
#endif

  fprintf_sysinfo(fp);

#if defined(__GNUC__) && defined(__GNUC_MINOR__) && defined(__GNUC_PATCHLEVEL__)
  fprintf(fp, "GCC version:                        %i.%i.%i\n",
          __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__);
#endif

#if defined (__GNU_LIBRARY__)
  fprintf(fp, "GNU C Library:                      %i\n", __GNU_LIBRARY__);
#endif

#if defined (__GLIBC__) && defined(__GLIBC_MINOR__)
  fprintf(fp, "glibc version:                      %i.%i\n",
          __GLIBC__, __GLIBC_MINOR__);
#endif

#if defined(NIDS_MAJOR) && defined(NIDS_MINOR)
  fprintf(fp, "Libnids version:                    %i.%i\n",
          NIDS_MAJOR, NIDS_MINOR);
#endif

  fprintf(fp, "Libnids have nids_getpcapdesc():    ");
#if defined(HAVE_NIDS_GETPCAPDESC)
  fprintf(fp, "yes\n");
#else
  fprintf(fp, "no\n");
#endif

#ifdef HAVE_PCAP_LIB_VERSION
  fprintf(fp, "Libpcap version string:             %s\n", pcap_lib_version());
#endif /* HAVE_PCAP_LIB_VERSION */

  fprintf(fp, "Default configuration file:         %s\n", DEFAULT_CONFIG_FILE);

  fprintf(fp, "TCP connection timeout:             ");
#ifdef ENABLE_TIMEOUT
  fprintf(fp, "enabled\n");
#else
  fprintf(fp, "disabled\n");
#endif

  fprintf(fp, "TCP connection statistics:          ");
#ifdef ENABLE_STATISTICS
  fprintf(fp, "enabled\n");
#else
  fprintf(fp, "disabled\n");
#endif

  fprintf(fp, "Performance statistics:             ");
#ifdef ENABLE_PERF_STATS
  fprintf(fp, "enabled\n");
#else
  fprintf(fp, "disabled\n");
#endif

  fprintf(fp, "floating point number precision:    ");
#ifdef ENABLE_DOUBLE
  fprintf(fp, "double (%lu bits)\n", sizeof (entropy_t) * 8);
#else
  fprintf(fp, "float (%lu bits)\n", sizeof (entropy_t) * 8);
#endif

  fprintf(fp, "Liam Paninski's entropy estimator:  ");
#ifdef ENABLE_PANINSKI
  fprintf(fp, "enabled\n");
  fprintf(fp, "Liam Paninski's estimator cache:    %s\n", PANINSKI_CACHE_FILE);

#ifdef HAVE_MPFR
  fprintf(fp, "MPFR version for Paninski bias:     %s\n", mpfr_get_version());
  fprintf(fp, "using MPFR precision (in bits):     %u\n", MPFR_PRECISION);
  fprintf(fp, "using MPFR rounding mode:           %s\n",
          mpfr_print_rnd_mode(MPFR_ROUNDING));
#endif

#else
  fprintf(fp, "disabled\n");
#endif

  fprintf(fp, "Have glob():                        ");
#if defined(HAVE_GLOB_H) && defined(HAVE_GLOB)
  fprintf(fp, "yes\n");
#else
  fprintf(fp, "no\n");
#endif

  fprintf(fp, "using alternate entropy code:       ");
#ifdef ENABLE_ALT_ENTROPY_CODE
  fprintf(fp, "yes\n");
#else
  fprintf(fp, "no\n");
#endif
}


/**
 * Empty syslog function to make libnids silent.
 **/
static void
quiet_nids_syslog(int type, int err, void *iph, void *data)
{
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
