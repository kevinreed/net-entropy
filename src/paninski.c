/**
 ** @file paninski.c
 ** Paninski bias correction for the Shannon entropy estimator.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Fri Oct 16 17:10:52 2009
 ** @date Last update: Thu Dec  5 00:19:31 2013
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#ifdef ENABLE_PANINSKI

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#ifdef HAVE_MPFR
#include <mpfr.h>
#endif /* HAVE_MPFR */

#include "net-entropy.h"

#include "paninski.h"
#include "log.h"

#include "paninski_priv.h"


static entropy_t *paninski_bias_cache_g = NULL;


/* MPFR version */
#if defined(HAVE_MPFR)

entropy_t
paninski_bias_mpfr_cached(int n)
{
  if (n < PANINSKI_CACHE_SIZE)
    return (paninski_bias_cache_g[n]);

  return (paninski_bias_mpfr_series(n));
}


/*
 * Computes: -log c + e^-c sum_{j>=1} c^j/j! log (j+1)
 * See the original work of Liam Paninski:
 * http://www.stat.columbia.edu/~liam/research/pubs/nm_proof.pdf
 * Many thanks to Jean Goubault-Larrecq for this function.
 */
static entropy_t
paninski_bias_mpfr_series(unsigned long n)
{
  mpfr_t c, z, cjj, zz, tmp;
  unsigned long j;
  entropy_t zval;

  mpfr_init2(zz, MPFR_PRECISION);
  mpfr_init2(tmp, MPFR_PRECISION);

  // c = n / 256.0;
  mpfr_init2(c, MPFR_PRECISION);
  mpfr_set_ui(c, n, MPFR_ROUNDING);
  mpfr_div_ui(c, c, 256, MPFR_ROUNDING);

  // z = 0;
  mpfr_init2(z, MPFR_PRECISION);
  mpfr_set_zero(z, 0);

  // cjj = c;
  mpfr_init2(cjj, MPFR_PRECISION);
  mpfr_set(cjj, c, MPFR_ROUNDING);

  j = 1;
  for ( ; ; ) {

    // zz = z + cjj * logl((long double)(j + 1));
    mpfr_set_ui(zz, j + 1, MPFR_ROUNDING);
    mpfr_log(zz, zz, MPFR_ROUNDING);
    mpfr_mul(zz, zz, cjj, MPFR_ROUNDING);
    mpfr_add(zz, zz, z, MPFR_ROUNDING);

    // if (zz == z)
    if ( mpfr_equal_p(zz, z) )
      break ;

    j++;

    // cjj *= c / (long double)j;
    mpfr_div_ui(tmp, c, j, MPFR_ROUNDING);
    mpfr_mul(cjj, cjj, tmp, MPFR_ROUNDING);

    // z = zz;
    mpfr_set(z, zz, MPFR_ROUNDING);
  }

  // z *= expl(-c);
  mpfr_neg(tmp, c, MPFR_ROUNDING);
  mpfr_exp(tmp, tmp, MPFR_ROUNDING);
  mpfr_mul(z, z, tmp, MPFR_ROUNDING);

  // z -= logl(c);
  mpfr_log(tmp, c, MPFR_ROUNDING);
  mpfr_sub(z, z, tmp, MPFR_ROUNDING);

  // z /= logl(2.0);
  mpfr_set_ui(tmp, 2, MPFR_ROUNDING);
  mpfr_log(tmp, tmp, MPFR_ROUNDING);
  mpfr_div(z, z, tmp, MPFR_ROUNDING);

#ifdef ENABLE_DOUBLE
  zval = mpfr_get_d(z, MPFR_ROUNDING);
#else
  zval = mpfr_get_flt(z, MPFR_ROUNDING);
#endif

  mpfr_clear(c);
  mpfr_clear(z);
  mpfr_clear(cjj);
  mpfr_clear(zz);
  mpfr_clear(tmp);

  return (zval);
}
#endif /* defined(HAVE_MPFR) */




/* long double version */
#if defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL)
entropy_t
paninski_bias_l_cached(int n)
{
  if (n < PANINSKI_CACHE_SIZE)
    return (paninski_bias_cache_g[n]);

  return ((entropy_t)paninski_bias_l((long double)n / 256.0));
}


static long double
paninski_bias_l(long double c)
{
  if (c >= 11350.0)
    return (paninski_bias_l_approx(c));
  else
    return (paninski_bias_l_series(c));
}


/*
 * Computes: -log c + e^-c sum_{j>=1} c^j/j! log (j+1)
 * See the original work of Liam Paninski:
 * http://www.stat.columbia.edu/~liam/research/pubs/nm_proof.pdf
 * Many thanks to Jean Goubault-Larrecq for this function.
 * This function will overflow for c >= 11355.
 */
static long double
paninski_bias_l_series(long double c)
{
  long double z, cjj, zz;
  int j;

  z = 0.0;
  cjj = c;
  for (j = 1 ; ; ) {
    zz = z + cjj * logl((long double)(j + 1));
    if (zz == z)
      break ;
    j++;
    cjj *= c / (long double)j;
    z = zz;
  }
  z *= expl(-c);
  z -= logl(c);

  return (z / logl(2.0));
}


/*
 * A crude but fast approximation of the Paninski's bias function.
 * This will do the job for large values of c.
 */
static long double
paninski_bias_l_approx(long double c)
{
  return (1.0 / logl(4.0) / c);
}
#endif /* defined(HAVE_LONG_DOUBLE_WIDER) && defined(HAVE_EXPL) && defined(HAVE_LOGL) */

/* double version */
#if defined(HAVE_EXP) && defined(HAVE_LOG)
entropy_t
paninski_bias_cached(int n)
{
  if (n < PANINSKI_CACHE_SIZE)
    return (paninski_bias_cache_g[n]);

  return ((entropy_t)paninski_bias((double)n / 256.0));
}


static double
paninski_bias(double c)
{
  if (c >= 700.0)
    return (paninski_bias_approx(c));
  else
    return (paninski_bias_series(c));
}


static double
paninski_bias_series(double c)
{
  double z, cjj, zz;
  int j;

  z = 0.0;
  cjj = c;
  for (j = 1 ; ; ) {
    zz = z + cjj * log((double)(j + 1));
    if (zz == z)
      break ;
    j++;
    cjj *= c / (double)j;
    z = zz;
  }
  z *= exp(-c);
  z -= log(c);

  return (z / log(2.0));
}


static double
paninski_bias_approx(double c)
{
  return (1.0 / log(4.0) / c);
}
#endif /* defined(HAVE_EXP) && defined(HAVE_LOG) */


/* float version */
#if defined(HAVE_EXPF) && defined(HAVE_LOGF)
entropy_t
paninski_bias_f_cached(int n)
{
  if (n < PANINSKI_CACHE_SIZE)
    return (paninski_bias_cache_g[n]);

  return ((entropy_t)paninski_bias_f((float)n / 256.0));
}


static float
paninski_bias_f(float c)
{
  if (c >= 85.0)
    return (paninski_bias_f_approx(c));
  else
    return (paninski_bias_f_series(c));
}


static float
paninski_bias_f_series(float c)
{
  float z, cjj, zz;
  int j;

  z = 0.0;
  cjj = c;
  for (j = 1 ; ; ) {
    zz = z + cjj * logf((float)(j + 1));
    if (zz == z)
      break ;
    j++;
    cjj *= c / (float)j;
    z = zz;
  }
  z *= expf(-c);
  z -= logf(c);

  return (z / logf(2.0));
}


static float
paninski_bias_f_approx(float c)
{
  return (1.0 / logf(4.0) / c);
}
#endif /* defined(HAVE_EXPF) && defined(HAVE_LOGF) */


static void
paninski_cache_save(void)
{
  FILE *fp;
  size_t ret;
  paninski_cache_hdr_t h;

  fp = fopen(PANINSKI_CACHE_FILE, "w");
  if (fp == NULL) {
    log_error("paninski_cache_save(): "
              "Could not open cache file '%s' for writing: "
              "error %i: %s\n",
              PANINSKI_CACHE_FILE, errno, strerror(errno));
    exit(EXIT_FAILURE);
  }

  strcpy(h.magic, PANINSKI_CACHE_MAGIC);
  h.version = PANINSKI_CACHE_VERSION;
  h.sizeof_float = sizeof (entropy_t);
  h.cache_size = PANINSKI_CACHE_SIZE;

  ret = fwrite(&h, sizeof (paninski_cache_hdr_t), 1, fp);
  if (ret != 1) {
    log_error("paninski_cache_save(): fwrite(): Could not write cache header.\n");
    exit(EXIT_FAILURE);
  }

  ret = fwrite(paninski_bias_cache_g,
               sizeof (entropy_t) * PANINSKI_CACHE_SIZE,
               1, fp);
  if (ret != 1) {
    log_error("paninski_cache_save(): fwrite(): Could not write cache data.\n");
    exit(EXIT_FAILURE);
  }

  fclose(fp);
}

static void
paninski_compute_cache(void)
{
  int i;

  for (i = 0; i < PANINSKI_CACHE_SIZE; i++) {
#if 0
    if (verbose_g && !(i % (PANINSKI_CACHE_SIZE / 50)))
      log_error("Paninski cache build: %4.2f%%\n",
                (float)(i*100)/(float)PANINSKI_CACHE_SIZE);
#endif
    paninski_bias_cache_g[i] = PANINSKI_BIAS(i);
  }
#if 0
  if (verbose_g)
    log_error("Paninski cache complete.\n");
#endif

  paninski_cache_save();
}

static int
paninski_cache_hrd_ok(FILE *fp)
{
  paninski_cache_hdr_t h;
  size_t ret;

  if (fp == NULL)
    return (0);

  ret = fread(&h, sizeof (h), 1, fp);
  if ( (ret == 1) &&
       (!strcmp(h.magic, PANINSKI_CACHE_MAGIC)) &&
       (h.version == PANINSKI_CACHE_VERSION) &&
       (h.sizeof_float == sizeof (entropy_t)) &&
       (h.cache_size == PANINSKI_CACHE_SIZE)) {
    return (1);
  }

  ret = fclose(fp);
  if (ret != 0) {
    log_error("paninski_cache_hdr_ok(): fclose(): %s.\n",
              strerror(errno));
    exit(EXIT_FAILURE);
  }

  return (0);
}

void
paninski_init_cache(void)
{
  FILE *fp;
  size_t ret;
  int recompute_cache;

  recompute_cache = 0;

#if 0
  if (verbose_g)
    log_error("Initializing the paninski bias cache.\n");
#endif

  paninski_bias_cache_g = malloc(sizeof (entropy_t) * PANINSKI_CACHE_SIZE);
  if (paninski_bias_cache_g == NULL) {
    log_error("paninski_init_cache(): Could not allocate memory: "
              "error %i: %s\n",
              errno, strerror(errno));
    exit(EXIT_FAILURE);
  }

  fp = fopen(PANINSKI_CACHE_FILE, "r");
  if (paninski_cache_hrd_ok(fp)) {
#if 0
    if (verbose_g)
      log_error("Paninski cache file found.\n");
#endif
    ret = fread(paninski_bias_cache_g,
                sizeof (entropy_t) * PANINSKI_CACHE_SIZE,
                1, fp);
    if (ret != 1) {
      log_error("paninski_init_cache(): error while reading cache.\n");
      recompute_cache = 1;
    }
    fclose(fp);
  }
  else {
    recompute_cache = 1;
  }

  if (recompute_cache)
    paninski_compute_cache();
}
#endif /* ENABLE_PANINSKI */


/*
**  Copyright (c) 2004-2009 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
