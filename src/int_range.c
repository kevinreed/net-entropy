/**
 ** @file int_range.c
 ** A simple int range list parser.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Oct 18 01:15:44 2009
 ** @date Last update: Sun Oct 18 01:15:44 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "int_range.h"
#include "int_range_priv.h"


static int rlp_erroffset_g;
static char rlp_errmsg_g[128];


int
fprintf_range(FILE *fp, int_range_t *r, size_t rlen)
{
  int ret;

  ret = 0;

  for ( ; rlen > 0; rlen--, r++) {
    if (r->start == r->end)
      ret += fprintf(fp, "%li%s", r->start, rlen == 1 ? "" : ",");
    else
      ret += fprintf(fp, "%li-%li%s",
		     r->start, r->end, rlen == 1 ? "" : ",");
  }

  return (ret);
}

void
range_enum(int_range_t *r, size_t rlen, rfunc_t f, void *farg)
{
  long i;
  long end;

  for ( ; rlen > 0; rlen--, r++) {
    end = r->end;
    for (i = r->start; i <= end; i++) {
      f(i, farg);
    }
  }
}

int
build_range_elmt(int_range_t **range, size_t *range_len, long start, long end)
{
  int_range_t *r;
  size_t rl;

  if (end < start) {
    return (1);
  }

  r = *range;
  rl = *range_len;

  if (rl == 0) {
    r = malloc(sizeof (int_range_t));
    r[0].start = start;
    r[0].end = end;
    rl++;
  }
  else {
    if (start <= r[ rl - 1].end) {
      return (2);
    }
    else if (start == (r[ rl - 1 ].end) + 1) {
      r[ rl - 1 ].end = end;
    }
    else {
      rl++;
      r = realloc(r, (rl) * sizeof (int_range_t));
      r[ rl - 1 ].start = start;
      r[ rl - 1 ].end = end;
    }
  }

  *range = r;
  *range_len = rl;

  return (0);
}

static int
get_next_tok(rlp_t *ctx)
{
  const char *str;
  char *endptr;

  str = ctx->pos;

  while (*str == ' ' || *str == '\t')
    str++;

  if (*str == ',') {
    ctx->tok_type = TOKEN_COMMA;
    ctx->pos = str + 1;
  }
  else if (*str == '-') {
    ctx->tok_type = TOKEN_DASH;
    ctx->pos = str + 1;
  }
  else if (isdigit(*str)) {
    long val;

    ctx->tok_type = TOKEN_DIGIT;
    val = strtol(str, &endptr, 10);
    if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
	|| (errno != 0 && val == 0)) {
      ctx->tok_type = TOKEN_UNKNOWN;
    }
    ctx->int_val = val;
    ctx->pos = endptr;
  }
  else if (*str == '\n' || *str == '\0') {
    ctx->tok_type = TOKEN_EOS;
  }
  else {
    ctx->tok_type = TOKEN_ERR_UNKNOWN;
  }

  return (ctx->tok_type);
}

static void
read_range(rlp_t *rlp)
{
  int tt;
  int val1;
  int val2;
  int ret;

  tt = get_next_tok(rlp);
  if (tt != TOKEN_DIGIT) {
    rlp->tok_type = TOKEN_ERR_SYNTAX;
    return ;
  }
  val1 = rlp->int_val;
  tt = get_next_tok(rlp);
  if (tt == TOKEN_COMMA || tt == TOKEN_EOS) {
    ret = build_range_elmt(&rlp->range, &rlp->range_size, val1, val1);
    if (ret != 0) {
      rlp->tok_type = TOKEN_ERR_RANGE;
    }
    return ;
  }
  else if (tt != '-') {
    rlp->tok_type = TOKEN_ERR_SYNTAX;
    return ;
  }

  tt = get_next_tok(rlp);
  if (tt != TOKEN_DIGIT) {
    rlp->tok_type = TOKEN_ERR_SYNTAX;
    return ;
  }
  val2 = rlp->int_val;
  ret = build_range_elmt(&rlp->range, &rlp->range_size, val1, val2);
  if (ret != 0) {
    rlp->tok_type = TOKEN_ERR_RANGE;
    return ;
  }

  get_next_tok(rlp);

  return ;
}

static void
rlp_set_error(rlp_t *ctx)
{
  rlp_erroffset_g = (ctx->pos - ctx->str) + 1;

  switch (ctx->tok_type) {

  case TOKEN_ERR_UNKNOWN:
    snprintf(rlp_errmsg_g, sizeof (rlp_errmsg_g),
	     "Parse error at character %i (unknown token).",
	     rlp_erroffset_g);
    break ;

  case TOKEN_ERR_RANGE:
    snprintf(rlp_errmsg_g, sizeof (rlp_errmsg_g),
	     "Range declaration error at character %i.  "
	     "Ranges MUST be sorted in increasing order "
	     "and MUST NOT overlap.  Ex: 2,3-5,7-11.",
	     rlp_erroffset_g);
    break ;

  default:
  case TOKEN_ERR_SYNTAX:
    snprintf(rlp_errmsg_g, sizeof (rlp_errmsg_g),
	     "Syntax error at character %i.",
	     rlp_erroffset_g);
    break ;
  }
}

int
parse_range_list(const char *str,
		 int_range_t **range,
		 size_t *range_size)
{
  rlp_t rlp;

  memset(&rlp, 0, sizeof (rlp));

  rlp.str = str;
  rlp.pos = str;

  rlp_errmsg_g[0] = '\0';

  do {
    read_range(&rlp);
  } while (rlp.tok_type == ',');

  if (rlp.tok_type != TOKEN_EOS) {
    rlp_set_error(&rlp);

    return (PARSE_ERROR);
  }

  *range = rlp.range;
  *range_size = rlp.range_size;

  return (PARSE_OK);
}

const char *
rlp_get_errmsg(void)
{
  return (rlp_errmsg_g);
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
