/**
 ** @file int_range.c
 ** A simple int range list parser.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Oct 18 01:15:44 2009
 ** @date Last update: Sun Oct 18 01:15:44 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifndef INT_RANGE_H
#define INT_RANGE_H

#include <stdio.h>
#include <sys/types.h>

#define PARSE_OK    0
#define PARSE_ERROR 1

typedef struct int_range_s int_range_t;
struct int_range_s {
  long start;
  long end;
};

typedef void (*rfunc_t)(long i, void *args);

int
fprintf_range(FILE *fp, int_range_t *r, size_t rlen);

void
range_enum(int_range_t *r, size_t rlen, rfunc_t f, void *farg);

int
build_range_elmt(int_range_t **range, size_t *range_len, long start, long end);

int
parse_range_list(const char *str,
		 int_range_t **range,
		 size_t *range_size);

const char *
rlp_get_errmsg(void);


#endif /* INT_RANGE_H */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
