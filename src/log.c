/**
 ** @file log.c
 ** Minimal error logging functions.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Oct 18 01:46:50 2009
 ** @date Last update: Sun Oct 18 01:46:50 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <syslog.h>
#include <limits.h>

#include "log.h"

static int log_output_g = LOG_ERROR_STDERR;

/**
 * Error logging function.  Redirect logging to stderr if net-entropy
 * is attached to a console or to syslog running in daemon.
 *
 * @param prio  A valid syslog() priority value.
 * @param fmt   Format string.
 * @param ap    Variable argument list.
 **/
void
log_message(const int prio, const char *fmt, va_list ap)
{
  char buffer[LINE_MAX];

  if (log_output_g == LOG_ERROR_SYSLOG) {
    vsnprintf(buffer, sizeof (buffer), fmt, ap);
    syslog(prio, buffer);
  }
  else {
    vfprintf(stderr, fmt, ap);
  }
}

void
log_error(const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_ERR, fmt, ap);
  va_end(ap);
}

void
log_warning(const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_WARNING, fmt, ap);
  va_end(ap);
}

void
log_notice(const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_NOTICE, fmt, ap);
  va_end(ap);
}

void
log_info(const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_INFO, fmt, ap);
  va_end(ap);
}

int
get_log_error_output(void)
{
  return (log_output_g);
}

void
set_log_error_output(const int output)
{
  if ((output != LOG_ERROR_STDERR) && (output != LOG_ERROR_SYSLOG))
    return ;

  log_output_g = output;
}


/*
**  Copyright (c) 2004-2009 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
