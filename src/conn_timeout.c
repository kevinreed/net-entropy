/**
 ** @file conn_timeout.c
 ** Functions for handling TCP connection timeouts.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Nov  1 22:43:30 2009
 ** @date Last update: Thu Jun 17 13:41:57 2010
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#ifdef ENABLE_TIMEOUT

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <nids.h>

#include "net-entropy.h"

#include "dtailq.h"
#include "log_event.h"
#include "port_track.h"
#include "rule_output.h"
#include "perf_stats.h"

#include "conn_timeout.h"

time_t timeout_g = DEFAULT_TIMEOUT;
time_t last_gc_g = 0;
con_list_t con_list_g = DTAILQ_HEAD_INITIALIZER(con_list_g);


/**
 * Remove a libnids TCP stream that have been inactive.
 *
 * @param curtime  The current time.
 * @param a_tcp    The current libnids TCP stream being processed. 
 *                 This parameter is used to see when the current connection
 *                 is removed.
 * @return 1 if the current connection has been removed, 0 otherwise.
 **/
int
destroy_inactive_connections(time_t curtime, struct tcp_stream *a_tcp)
{
  stats_t *elmt;
  stats_t *next_elmt;
  time_t timedelta;
  int del_cur_conn;

  last_gc_g = curtime;
  del_cur_conn = 0;

  DTAILQ_FOREACH_SAFE(elmt, &con_list_g, cons, next_elmt) {
    timedelta = curtime - elmt->last_act;
    if (timedelta > timeout_g) {
      elmt->tcp->client.collect = 0;
      elmt->tcp->server.collect = 0;
      if (elmt->tcp == a_tcp) {
        del_cur_conn = 1;
      }
      if ( elmt->alarm ) {
        log_timeout(elmt->tcp, elmt);
      }
      perf_stats_tcp_timeout();
      DTAILQ_REMOVE(&con_list_g, elmt, cons);
#ifdef ENABLE_STATISTICS
      if (statdir_g && elmt->file) {
        if (learn_mode_g && (elmt->alarm & ALARM_UPDATE))
          fprintf(elmt->file,
                  "# Info: This connection updated rule\n");
        fprintf(elmt->file,
                "# Stop tracking last_act=%li > timeout=%li packets=%i\n",
                (long int)(curtime - elmt->last_act),
                (long int)timeout_g,
                elmt->packets);
        fclose(elmt->file);
      }
#endif /* ENABLE_STATISTICS */
      if (learn_mode_g && (elmt->alarm & ALARM_UPDATE))
        update_rule(elmt->tcp, ptbl_g);
      free(elmt);
    }
  }

  return (del_cur_conn);
}


#endif /* ENABLE_TIMEOUT */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
