/**
 ** @file misc.c
 ** Misc functions.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Tue Nov  3 21:46:23 2009
 ** @date Last update: Tue Nov  3 21:56:58 2009
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "net-entropy.h"

#include "log.h"
#include "misc.h"

/**
 * Redirect stdout and stderr to /dev/null or another file.
 *
 * @param stdout_file  Path to the file where stdout will be redirected.
 * @param stderr_file  Path to the file where stderr will be redirected.
 **/
void
daemonize(const char *stdout_file, const char *stderr_file)
{
  pid_t pid;
  int stdout_fd;
  int stderr_fd;
  char *devnull_str = "/dev/null";

  if (stdout_file == NULL)
    stdout_file = devnull_str;
    
  if (stderr_file == NULL)
    stderr_file = devnull_str;

  if ( !strcmp(stdout_file,stderr_file) && strcmp("/dev/null", stdout_file)) {
    log_error("WARNING, log may be corrupted if buffered I/O are used.\n");
    /* XXX: activate line buffer ?? */
    /* setline(stdout); */
    /* or, more portable */
    /* setvbuf(stdout, (char *)NULL, _IOLBF, 0); */
  }

  if (getppid() != 1) {
    pid = fork();

    if(pid > 0)
      exit(EXIT_SUCCESS);

    if(pid < 0) {
      log_error("fork(): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
    setsid();
  }

  if (chdir("/") != 0) {
      log_error("chdir(): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
  }

  umask(077);

  stdout_fd = open(stdout_file, O_CREAT | O_RDWR, S_IWUSR|S_IRUSR);
  close(STDOUT_FILENO);
  dup2(stdout_fd, STDOUT_FILENO);
  close(stdout_fd);

  stderr_fd = open(stderr_file, O_CREAT | O_RDWR, S_IWUSR|S_IRUSR);
  close(STDERR_FILENO);
  dup2(stderr_fd, STDERR_FILENO);
  close(stderr_fd);

  daemon_g = DAEMON_RUNNING;
}



/**
 * Drop root privilege by switching to another user.
 *
 * @param username  The username to chage to.
 **/
void
change_run_id(char *username)
{
  struct passwd *pwent;
  int ret;

  pwent = getpwnam(username);
  if (pwent == NULL) {
    log_error("error: user '%s' doesn't exist.\n", username);
    exit(EXIT_FAILURE);
  }

  if (verbose_g) {
    log_error("changing run uid to '%s' (uid=%i gid=%i)\n",
              username, pwent->pw_uid, pwent->pw_gid);
  }

  ret = setgid(pwent->pw_gid);
  if (ret != 0) {
    log_error("setgid(): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  ret = setuid(pwent->pw_uid);
  if (ret != 0) {
    log_error("setuid(): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}


/**
 * Set the memory limit (RLIMIT_AS).
 *
 * @param limit  The memory limit, in bytes.
 **/
void
set_memory_limit(int limit)
{
  struct rlimit lim;
  int ret;

  lim.rlim_cur = limit;
  lim.rlim_max = limit;

  ret = setrlimit(RLIMIT_AS, &lim );

  if (ret) {
    log_error("setrlimit: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}


/**
 * Set the priority of the current process.
 *
 * @param prio  The requested priority.
 **/
void
set_process_priority(int prio)
{
  int ret;

  ret = setpriority(PRIO_PROCESS, 0, prio);
  if (ret != 0)
    log_error("setpriority(%i): error %i: %s\n",
              prio, errno, strerror(errno));
}


/**
 * Return a string describing the analysis mode.
 *
 * @param type  The type of an analysis (in the global port track table ptbl_g).
 * @return A string describing the analysis mode.
 **/
const char *
get_dir_string(int type)
{
  if ((TRACK_DIR(type) == TRACK_BOTH) && ( ! TRACK_IS_CUMUL(type)) )
    return ("<->");
  if ((TRACK_DIR(type) == TRACK_BOTH) && ( TRACK_IS_CUMUL(type)) )
    return ("<=>");
  if ((TRACK_DIR(type) == TRACK_CLI2SRV) && ( ! TRACK_IS_CUMUL(type)) )
    return ("->");
  if ((TRACK_DIR(type) == TRACK_CLI2SRV) && ( TRACK_IS_CUMUL(type)) )
    return ("=>");
  if ((TRACK_DIR(type) == TRACK_SRV2CLI) && ( ! TRACK_IS_CUMUL(type)) )
    return ("<-");
  if ((TRACK_DIR(type) == TRACK_SRV2CLI) && ( TRACK_IS_CUMUL(type)) )
    return ("<=");

  return ("unknown");
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
