/**
 ** @file cfg_file.c
 ** File confguration parsing functions.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Mon Oct 16 23:23:23 2009
 ** @date Last update: Fri Jul  2 23:27:00 2010
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <nids.h>
#include <limits.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#if defined(HAVE_GLOB_H) && defined(HAVE_GLOB)
#include <glob.h>
#endif

#include "net-entropy.h"

#include "cfg_file.h"
#include "int_range.h"
#include "port_track.h"
#include "log.h"
#include "perf_stats.h"

static void
load_port_range(port_track_t *tbl,
		const port_track_t *template);
static int
port_range_list_ok(int_range_t *r, size_t rl);

static void
check_range(const char *file, const int line, const range_t *ranges, const int zn);

static int
line_begin_match(const char *match, const char *line);

static char *
absolutepath(const char *file);

static char *
load_pcap_filter_from_file(const char *file);


void
read_config_file(const char *file)
{
  char buffer[8192];
  char param[8192];
  int line;
  FILE *fp;
  char *l;

  line = 0;

  fp = fopen(file, "r");
  if (fp == NULL) {
    log_error("read_config_file(): fopen(\"%s\",\"r\"): error %i: %s\n",
              file, errno, strerror(errno));
    exit(EXIT_FAILURE);
  }

  while ( fgets(buffer, sizeof (buffer), fp) ) {
    line++;

    for (l = buffer; *l == ' ' || *l == '\t'; l++)
      ;
    if (*l == '\n' || *l == '#') {
      continue;
    }

    if (line_begin_match("ProtoSpec:", l)) {
      if (sscanf(l, "ProtoSpec: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad ProtoSpec token\n",
                file, line);
      }
      ports_g++;
#if defined(HAVE_GLOB_H) && defined(HAVE_GLOB)
      read_spec_files(param);
#else
      read_spec_file(ptbl_g, param);
#endif
    }
    else if (line_begin_match("MemoryLimit:", l)) {
      if (sscanf(l, "MemoryLimit: %u", &memory_limit_g) != 1) {
        log_error("read_config_file(): %s:%i: Bad MemoryLimit token\n",
                file, line);
      }
      memory_limit_g <<= 10; /* mul 1k */
    }
    else if (line_begin_match("PcapFilter:", l)) {
      nids_params.pcap_filter = strdup(l + strlen("PcapFilter:"));
    }
    else if (line_begin_match("PcapFilterFile:", l)) {
      if (sscanf(l, "PcapFilterFile: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad PcapFilterFile token\n",
                file, line);
      }
      nids_params.pcap_filter = load_pcap_filter_from_file(param);
    }
    else if (line_begin_match("RuntimeUser:", l)) {
      if (sscanf(l, "RuntimeUser: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad RuntimeUser token\n",
                file, line);
      }
      runtime_user_g = strdup(param);
    }
    else if (line_begin_match("ProcessPriority:", l)) {
      if (sscanf(l, "ProcessPriority: %i", &priority_g) != 1) {
        log_error("read_config_file(): %s:%i: Bad ProcessPriority token\n",
                file, line);
      }
    }
    else if (line_begin_match("Interface:", l)) {
      if (sscanf(l, "Interface: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad Interface token\n",
                  file, line);
      }
      nids_params.device = strdup(param);
    }
    else if (line_begin_match("MaxTrackSize:", l)) {
      if (sscanf(l, "MaxTrackSize: %u", &max_tracking_size_g) != 1) {
        log_error("read_config_file(): %s:%i: Bad MaxTrackSize token\n",
                file, line);
      }
    }
#ifdef ENABLE_TIMEOUT
    else if (line_begin_match("ConnTimeout:", l)) {
      if (sscanf(l, "ConnTimeout: %u", (unsigned int *)&timeout_g) != 1) {
        log_error("read_config_file(): %s:%i: Bad ConnTimeout token\n",
                file, line);
      }
    }
#endif /* ENABLE_TIMEOUT */
#ifdef ENABLE_PERF_STATS
    else if (line_begin_match("PerfStatsFile:", l)) {
      if (sscanf(l, "PerfStatsFile: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad PerfStatsFile token\n",
                  file, line);
      }
      perf_stats_open_file(param);
    }
    else if (line_begin_match("PerfStatsPktTimeout:", l)) {
      unsigned int t;
      if (sscanf(l, "PerfStatsPktTimeout: %u", &t) != 1) {
        log_error("read_config_file(): %s:%i: Bad PerfStatsPktTimeout token\n",
                  file, line);
      }
      perf_stats_set_timeout(t);
    }
    else if (line_begin_match("PerfStatsPktCount:", l)) {
      int c;
      if (sscanf(l, "PerfStatsPktCount: %u", &c) != 1) {
        log_error("read_config_file(): %s:%i: Bad PerfStatsPktCount token\n",
                  file, line);
      }
      perf_stats_set_call_limit(c);
    }
    else if (line_begin_match("PerfStatsFormat:", l)) {
      if (sscanf(l, "PerfStatsFormat: %s", param) != 1) {
        log_error("read_config_file(): %s:%i: Bad PerfStatsFormat token\n",
                  file, line);
      }
      perf_stats_set_fmt(param);
    }
#endif /* ENABLE_PERF_STATS */
    else {
      log_error("read_spec_file(): %s:%i: Syntax error, ignoring line\n",
                file, line);
    }
  }
  fclose(fp);
}


#if defined(HAVE_GLOB_H) && defined(HAVE_GLOB)

static int
spec_glob_error(const char *epath, int eerrno)
{
  log_error("read_spec_files(): "
            "error %i in glob() expansion: path %s: %s",
            eerrno, epath, strerror(eerrno));

  return (0);
}


void
read_spec_files(const char *pattern)
{
  int ret;
  int i;
  glob_t globbuf;

  ret = glob(pattern, 0, spec_glob_error, &globbuf);
  if (ret) {
    if (ret == GLOB_NOMATCH) {
      log_error("read_spec_files(): "
                "warning: Pattern '%s' returned no match.\n",
                pattern);
    }
    else {
      log_error("read_spec_files(): glob() error.\n");
    }
  }

  for (i = 0; i < globbuf.gl_pathc; i++) {
    read_spec_file(ptbl_g, globbuf.gl_pathv[i]);
  }

  globfree(&globbuf);
}
#endif /* defined(HAVE_GLOB_H) && defined(HAVE_GLOB) */


/**
 * Quick'n'dirty (tm) specification file parser.
 *
 * @param tbl  The port table to fill.
 * @param file The file to load.
 **/
void
read_spec_file(port_track_t *tbl, const char *file)
{
  char buffer[8192];
  char *l;
  FILE *fp;
  int line;
  int_range_t *port_range;
  size_t port_range_len;
  range_t *ranges;
  int zn;
  int type;
  int ret;
  port_track_t cpt;

  fp = fopen(file, "r");
  if (fp == NULL) {
    log_error("read_spec_file(): fopen(\"%s\",\"r\"): error %i: %s\n",
              file, errno, strerror(errno));
    exit(EXIT_FAILURE);
  }

  type = DEFAULT_TRACK_TYPE;
  ranges = NULL;
  zn = 0;
  port_range = NULL;
  port_range_len = 0;
  line = 0;

  while ( fgets(buffer, sizeof (buffer), fp) ) {
    line++;

    for (l = buffer; *l == ' ' || *l == '\t'; l++)
      ;
    if (*l == '\n' || *l == '#') {
      continue;
    }

    if      (line_begin_match("Port: all", l) ||
	     line_begin_match("Port: any", l)) {
      build_range_elmt(&port_range, &port_range_len, 0, MAX_PORTS);
    }
    else if (line_begin_match("Port:", l)) {
      ret = parse_range_list((l + strlen("Port:")),
			     &port_range, &port_range_len);
      if (ret != PARSE_OK) {
        log_error("read_spec_file(): %s:%i: "
		  "bad argument to Port directive (%s)\n",
                  file, line, rlp_get_errmsg());
	if (port_range)
	  free(port_range);
	port_range_len = 0;
      }
      else {
        if ( !port_range_list_ok(port_range, port_range_len) ) {
          log_error("read_spec_file(): %s:%i: "
                    "Some Port arguments are out of range (0-%u)\n",
                    file, line, MAX_PORTS);
	  if (port_range)
	    free(port_range);
	  port_range_len = 0;
        }
      }
    }
    else if (line_begin_match("Direction: both", l)) {
      type = (type & ~TRACK_DIR_MASK) | TRACK_BOTH;
    }
    else if (line_begin_match("Direction: cli2srv", l)) {
      type = (type & ~TRACK_DIR_MASK) | TRACK_CLI2SRV;
    }
    else if (line_begin_match("Direction: srv2cli", l)) {
      type = (type & ~TRACK_DIR_MASK) | TRACK_SRV2CLI;
    }
    else if (line_begin_match("Cumulative: yes", l)) {
      type = (type & ~TRACK_CUMUL_MASK) | TRACK_CUMULATIVE;
    }
    else if (line_begin_match("Cumulative: no", l)) {
      type = (type & ~TRACK_CUMUL_MASK) | TRACK_PERPACKET;
    }
    else if (line_begin_match("RangeUnit: bytes", l)) {
      type = (type & ~TRACK_TYPE_MASK) | TRACK_BYTES;
    }
    else if (line_begin_match("RangeUnit: packets", l)) {
      type = (type & ~TRACK_TYPE_MASK) | TRACK_PACKETS;
    }
    else if (line_begin_match("Entropy: shannon", l)) {
      type = (type & ~TRACK_ESTIMATOR_MASK) | TRACK_SHANNON;
    }
#ifdef ENABLE_PANINSKI
    else if (line_begin_match("Entropy: paninski", l)) {
      type = (type & ~TRACK_ESTIMATOR_MASK) | TRACK_PANINSKI;
    }
#endif /* ENABLE_PANINSKI */
    else if (line_begin_match("Range:", l)) {
      ranges = realloc(ranges, (zn+1) * sizeof (range_t));
      ret = sscanf(l, "Range: %li %li %" ENT_FMT " %" ENT_FMT,
             &ranges[zn].start, &ranges[zn].end,
             &ranges[zn].ent_min, &ranges[zn].ent_max);
      if (ret != 4) {
        log_error("read_spec_file(): %s:%i: Syntax error, ignoring line\n",
                  file, line);
      }
      check_range(file, line, ranges, zn);
      zn++;
    }
    else {
      log_error("read_spec_file(): %s:%i: Syntax error, ignoring line\n",
                file, line);
    }
  }

  fclose(fp);

  if (port_range_len == 0) {
    log_error("read_spec_file(): %s: Missing Port directive. Ignoring file.\n",
              file);
    if (ranges)
      free(ranges);

    return ;
  }

  if (zn == 0) {
    log_error("read_spec_file(): %s: No range defined. Ignoring file\n", file);
    if (port_range)
      free(port_range);

    return ;
  }

  cpt.type = type;
  cpt.ranges = ranges;
  cpt.range_num = zn;
  cpt.file = absolutepath(file);
  cpt.port_def = port_range;
  cpt.port_def_size = port_range_len;

  load_port_range(tbl, &cpt);
}


static int
line_begin_match(const char *match, const char *line)
{
  return (!strncmp(line, match, strlen(match)));
}


static char *
absolutepath(const char *file)
{
  char filepath[PATH_MAX];
  char *ret;

  ret = realpath(file, filepath);
  if (ret == NULL) {
    log_error("realpath('%s'):error %i: %s\n",
              file, errno, strerror(errno));
    return (NULL);
  }

  ret = strdup(filepath);

  return (ret);
}


static void
load_port_range(port_track_t *tbl,
		const port_track_t *template)
{
  long i;
  long end;
  int_range_t *r;
  size_t rlen;

  for ( r = template->port_def, rlen = template->port_def_size;
	rlen > 0;
	rlen--, r++) {
    end = r->end;
    for (i = r->start; i <= end; i++) {
      if (TRACK_TYPE(tbl[i].type) != TRACK_NOTHING) {
	log_error("load_port_range(): ERROR: "
		  "rule '%s' is redefining port %i already defined "
		  "in '%s'.  Ignoring this port.\n",
		  template->file, i, tbl[i].file);
      }
      else {
	memcpy(&tbl[i], template, sizeof (port_track_t));
      }
    }
  }
}


static int
port_range_list_ok(int_range_t *r, size_t rl)
{
  long v;

  for ( ; rl > 0; rl--, r++) {
    v = r->start;
    if (v < 0 || v >= MAX_PORTS)
      return (0);
    v = r->end;
    if (v < 0 || v >= MAX_PORTS)
      return (0);
  }

  return (1);
}



/**
 * Check that a range specification is valid.
 *
 * @param file    The corresponding rule file.
 * @param line    The line in the file.
 * @param ranges  Ranges defined in the corresponding file.
 * @param zn      The range to check.
 **/
static void
check_range(const char *file, const int line, const range_t *ranges, const int zn)
{
  if (ranges[zn].start > ranges[zn].end) {
    log_error("read_spec_file(): %s:%i: "
              "range start MUST be lower than range end\n",
              file, line);
  }

  if (zn > 0 && ranges[zn-1].end >= ranges[zn].start) {
    log_error("read_spec_file(): %s:%i: Ranges MUST be sorted AND MUST NOT overlap\n",
              file, line);
  }

  if (ranges[zn].ent_min > ranges[zn].ent_max) {
    log_error("read_spec_file(): %s:%i: "
              "minimal entropy (%f) MUST be less that maximal entropy (%f)\n",
              file, line, ranges[zn].ent_min, ranges[zn].ent_max);
  }

  if (ranges[zn].start < 256 && ranges[zn].ent_min > LOG2(ranges[zn].start)) {
    log_error("read_spec_file(): %s:%i: "
              "Warning: statistical entropy can't exceed %f "
              "for a message of size %li (read %f).\n",
              file, line,
              LOG2(ranges[zn].start), ranges[zn].start, ranges[zn].ent_min);
  }
  else if (ranges[zn].ent_min > 8.0 || ranges[zn].ent_min < 0.0) {
    log_error("read_spec_file(): %s:%i: "
              "Warning: , statistical entropy can't exceed 8.0 "
              " (read %f).\n",
              file, line, ranges[zn].ent_min);
  }

  if (ranges[zn].end < 256 && ranges[zn].ent_max > LOG2(ranges[zn].end)) {
    log_error("read_spec_file(): %s:%i: "
              "Warning: statistical entropy can't exceed %f "
              "for a message of size %li (read %f).\n",
              file, line,
              LOG2(ranges[zn].end), ranges[zn].end, ranges[zn].ent_max);
  }
  else if (ranges[zn].ent_max > 8.0 || ranges[zn].ent_max < 0.0) {
    log_error("read_spec_file(): %s:%i: "
              "Warning: statistical entropy can't exceed 8.0 "
              " (read %f).\n",
              file, line, ranges[zn].ent_max);
  }
}


static void
strip_newlines(char *filter, off_t length)
{
  for ( ; length > 0; length--) {
    if (*filter == '\n')
      *filter = ' ';
    filter++;
  }
}


static char *
load_pcap_filter_from_file(const char *file)
{
  int ret;
  char *filter;
  struct stat file_stat;
  FILE *fp;

  fp = fopen(file, "r");
  if (fp == NULL) {
    log_error("load_pcap_filter_from_file(): error while opening '%s': %s.\n",
              file, strerror(errno));
    return (NULL);
  }

  ret = fstat(fileno(fp), &file_stat);
  if (ret != 0) {
    log_error("load_pcap_filter_from_file(): fstat() error: %s\n",
              strerror(errno));
    fclose(fp);
    return (NULL);
  }

  if (file_stat.st_size == 0) {
    fclose(fp);
    return (NULL);
  }

  filter = malloc(file_stat.st_size + 1);
  if (filter == NULL) {
    log_error("load_pcap_filter_from_file(): malloc() error: %s\n",
              strerror(errno));
    fclose(fp);
    return (NULL);
  }

  ret = fread(filter, file_stat.st_size, 1, fp);
  if (ret != 1) {
    log_error("load_pcap_filter_from_file(): error while reading file: %s\n");
    free(filter);
    fclose(fp);
    return (NULL);
  }

  filter[ file_stat.st_size ] = '\0';

  strip_newlines(filter, file_stat.st_size);

  return (filter);
}



/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
