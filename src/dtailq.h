/**
 ** @file dtailq.h
 ** List macros.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Wed Mar 17 02:16:12 2004
 ** @date Last update: Tue Oct 11 15:54:36 2005
 **/


#ifndef DTAILQ_H
#define DTAILQ_H

/*
 * Tail queue declarations.
 */
#define DTAILQ_HEAD(name, type_t)                                             \
struct dtailq_##name##_s {                                                    \
  type_t  *tqh_first;                                                         \
  type_t **tqh_last;                                                          \
}

#define DTAILQ_HEAD_INITIALIZER(head)                                         \
  { NULL, &(head).tqh_first }

#define DTAILQ_ENTRY(type_t)                                                  \
struct {                                                                      \
  type_t  *tqe_next;                                                          \
  type_t **tqe_prev;                                                          \
}

/*
 * Tail queue functions.
 */
#define DTAILQ_CONCAT(head1, head2, field)                                    \
do {                                                                          \
  if (!DTAILQ_IS_EMPTY(head2)) {                                              \
    *(head1)->tqh_last = (head2)->tqh_first;                                  \
    (head2)->tqh_first->field.tqe_prev = (head1)->tqh_last;                   \
    (head1)->tqh_last = (head2)->tqh_last;                                    \
    DTAILQ_INIT((head2));                                                     \
  }                                                                           \
} while (0)

#define DTAILQ_IS_EMPTY(head) ((head)->tqh_first == NULL)

#define DTAILQ_FIRST(head) ((head)->tqh_first)

#define DTAILQ_FOREACH(var, head, field)                                      \
  for ((var) = DTAILQ_FIRST((head));                                          \
       (var);                                                                 \
       (var) = DTAILQ_NEXT((var), field))

#define DTAILQ_FOREACH_SAFE(var, head, field, tvar)                           \
  for ((var) = DTAILQ_FIRST((head));                                          \
       (var) && ((tvar) = DTAILQ_NEXT((var), field), 1);                      \
       (var) = (tvar))

#define DTAILQ_FOREACH_REVERSE(var, head, headname, field)                    \
  for ((var) = DTAILQ_LAST((head), headname);                                 \
       (var);                                                                 \
       (var) = DTAILQ_PREV((var), headname, field))

#define DTAILQ_FOREACH_REVERSE_SAFE(var, head, headname, field, tvar)         \
  for ((var) = DTAILQ_LAST((head), headname);                                 \
       (var) && ((tvar) = DTAILQ_PREV((var), headname, field), 1);            \
       (var) = (tvar))

#define DTAILQ_INIT(head)                                                     \
do {                                                                          \
  DTAILQ_FIRST((head)) = NULL;                                                \
  (head)->tqh_last = &DTAILQ_FIRST((head));                                   \
} while (0)

#define DTAILQ_INSERT_AFTER(head, listelm, elm, field)                        \
do {                                                                          \
  if ((DTAILQ_NEXT((elm), field) = DTAILQ_NEXT((listelm), field)) != NULL)    \
    DTAILQ_NEXT((elm), field)->field.tqe_prev = &DTAILQ_NEXT((elm), field);   \
  else {                                                                      \
    (head)->tqh_last = &DTAILQ_NEXT((elm), field);                            \
  }                                                                           \
  DTAILQ_NEXT((listelm), field) = (elm);                                      \
  (elm)->field.tqe_prev = &TAILQ_NEXT((listelm), field);                      \
} while (0)

#define DTAILQ_INSERT_BEFORE(listelm, elm, field)                             \
do {                                                                          \
  (elm)->field.tqe_prev = (listelm)->field.tqe_prev;                          \
  DTAILQ_NEXT((elm), field) = (listelm);                                      \
  *(listelm)->field.tqe_prev = (elm);                                         \
  (listelm)->field.tqe_prev = &DTAILQ_NEXT((elm), field);                     \
} while (0)

#define DTAILQ_INSERT_HEAD(head, elm, field)                                  \
do {                                                                          \
  if ((DTAILQ_NEXT((elm), field) = TAILQ_FIRST((head))) != NULL)              \
    DTAILQ_FIRST((head))->field.tqe_prev = &DTAILQ_NEXT((elm), field);        \
  else                                                                        \
    (head)->tqh_last = &DTAILQ_NEXT((elm), field);                            \
  DTAILQ_FIRST((head)) = (elm);                                               \
  (elm)->field.tqe_prev = &DTAILQ_FIRST((head));                              \
} while (0)

#define DTAILQ_INSERT_TAIL(head, elm, field)                                  \
do {                                                                          \
  DTAILQ_NEXT((elm), field) = NULL;                                           \
  (elm)->field.tqe_prev = (head)->tqh_last;                                   \
  *(head)->tqh_last = (elm);                                                  \
  (head)->tqh_last = &DTAILQ_NEXT((elm), field);                              \
} while (0)

#define DTAILQ_LAST(head, headname)                                           \
  (*(((struct dtailq_##headname##_t *)((head)->tqh_last))->tqh_last))

#define DTAILQ_NEXT(elm, field) ((elm)->field.tqe_next)

#define DTAILQ_PREV(elm, hname, field)                                        \
  (*(((struct dtailq_##hname##_t *)((elm)->field.tqe_prev))->tqh_last))

#define DTAILQ_REMOVE(head, elm, field)                                       \
do {                                                                          \
  if ((DTAILQ_NEXT((elm), field)) != NULL)                                    \
    DTAILQ_NEXT((elm), field)->field.tqe_prev = (elm)->field.tqe_prev;        \
  else {                                                                      \
    (head)->tqh_last = (elm)->field.tqe_prev;                                 \
  }                                                                           \
  *(elm)->field.tqe_prev = DTAILQ_NEXT((elm), field);                         \
} while (0)

#endif /* DTAILQ_H */
