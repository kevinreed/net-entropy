#ifndef PERF_STATS_H
#define PERF_STATS_H

# ifdef ENABLE_PERF_STATS

typedef struct perf_stats_s perf_stats_t;
struct perf_stats_s {
  struct timeval start_time;
  unsigned long current_tcp_conn;
  unsigned long total_tcp_conn;
  unsigned long tcp_pkt;
  unsigned long tcp_data_pkt;
  unsigned long tcp_timeout;
  unsigned long tcp_size_limit;
  unsigned long tcp_closed;

  char *fmtstr;
  char *file;
  FILE *fp;
  unsigned int call_count;
  unsigned int call_limit;
  time_t perf_stat_time;
  time_t last_call_time;
};

/*
** linux (2.4) process informations in /proc/<pid>/stat
** see manpage proc(5) for more informations
*/
typedef struct linux_process_info_s linux_process_info_t;
struct linux_process_info_s
{
  pid_t         pid;
  char          comm[16];
  char          pad_for_state[3];
  char          state;
  pid_t         ppid;
  pid_t         pgrp;
  pid_t         session;
  int           tty_nr;
  pid_t         tpgid;
  unsigned long flags;
  unsigned long minflt;
  unsigned long cminflt;
  unsigned long majflt;
  unsigned long cmajflt;
  unsigned long utime;
  unsigned long stime;
  long          cutime;
  long          cstime;
  long          priority;
  long          nice;
  long          itrealvalue;
  unsigned long starttime;
  unsigned long vsize;
  long          rss;
  unsigned long rlim;
  unsigned long startcode;
  unsigned long endcode;
  unsigned long startstack;
  unsigned long kstkesp;
  unsigned long kstkeip;
  unsigned long signal;
  unsigned long blocked;
  unsigned long sigignore;
  unsigned long sigcatch;
  unsigned long wchan;
  unsigned long nswap;
  unsigned long cnswap;
  int           exit_signal;
  int           processor;
};

void
perf_stats_init(void);

void
perf_stats_open_file(const char *file);

void
perf_stats_set_timeout(time_t timeout);

void
perf_stats_set_call_limit(int call_limit);

void
perf_stats_set_fmt(const char *fmt);

void
perf_stats_dispose(void);

void
perf_stats_inc_tcp_conn(void);

void
perf_stats_dec_tcp_conn(void);

void
perf_stats_tcp_pkt(void);

void
perf_stats_tcp_data_pkt(void);

void
perf_stats_tcp_timeout(void);

void
perf_stats_tcp_size_limit(void);

void
perf_stats_tcp_closed(void);

void
perf_stats(time_t t);

int
fprintf_stats(FILE *fp, const char *fmt);

void
get_linux_process_info(linux_process_info_t *p, pid_t pid);

# else /* ENABLE_PERF_STATS */

#  define perf_stats(t)
#  define perf_stats_init()
#  define perf_stats_inc_tcp_conn()
#  define perf_stats_dec_tcp_conn()
#  define perf_stats_tcp_pkt()
#  define perf_stats_tcp_data_pkt()
#  define perf_stats_tcp_timeout()
#  define perf_stats_tcp_size_limit()
#  define perf_stats_tcp_closed()

# endif /* ENABLE_PERF_STATS */

#endif /* PERF_STATS_H */
