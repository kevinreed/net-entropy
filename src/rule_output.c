/**
 ** @file rule_ourput.c
 ** Functions for printing parsable rules.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Sun Nov  1 23:10:16 2009
 ** @date Last update: Thu May 27 13:42:13 2010
 **/

/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <syslog.h>
#include <nids.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

#include "net-entropy.h"

#include "port_track.h"
#include "log.h"

#include "rule_output.h"


/**
 * Update a rule file with new learned values.
 *
 * @param tcp    The current libnids TCP stream.
 * @param table  A pointer to the track table (ptbl_g).
 **/
void
update_rule(tcp_stream_t *tcp, port_track_t *table)
{
  char bak[PATH_MAX];
  struct timeval tv;
  char *fname;
  FILE *fp;
  int ret;
  int port;

  port = tcp->addr.dest;
  fname = table[port].file;

  if (!delete_old_rules_g) {
    gettimeofday(&tv, NULL);
    snprintf(bak, sizeof (bak), "%s.%li.%06li",
             fname, (long int)tv.tv_sec, (long int)tv.tv_usec);
    ret = rename(fname, bak);
    if (ret < 0) {
      log_error("rename(\"%s\",\"%s\"): error %i: %s\n",
                fname, bak, errno, strerror(errno));
    }
  }
  fp = fopen(fname, "w");
  if (fp == NULL) {
    log_error("fopen(\"%s\", \"w\"): error %i: %s\n",
              fname, errno, strerror(errno));
    return ;
  }
  fprintf_rule(fp, &table[port]);
  fclose(fp);

  syslog(LOG_NOTICE, "Updated rule file '%s'\n", fname);
}


/**
 * Print a rule on a stream.
 *
 * @param fp    FILE stream to write on.
 * @param rule  Rule to print.
 **/
void
fprintf_rule(FILE *fp, port_track_t *rule)
{
  int i;
  int z;

  z = rule->range_num;

  fprintf(fp, 
          "#\n"
          "# File automatically generated by Net-Entropy\n"
          "#\n\n");

  fprintf(fp, "Port: ");
  fprintf_range(fp, rule->port_def, rule->port_def_size);
  fprintf(fp, "\n");

  fprintf(fp, "Direction: ");
  switch (TRACK_DIR(rule->type)) {
  case TRACK_CLI2SRV:
    fprintf(fp, "cli2srv\n");
    break;
  case TRACK_SRV2CLI:
    fprintf(fp, "srv2cli\n");
    break;
  case TRACK_BOTH:
    fprintf(fp, "both\n");
    break;
  default:
    fprintf(fp, "unknown\n");
    break;
  }

  fprintf(fp, "Cumulative: %s\n", TRACK_IS_CUMUL(rule->type) ? "yes":"no");

  fprintf(fp, "Entropy: ");
  switch (TRACK_ESTIMATOR(rule->type)) {
  case TRACK_SHANNON:
    fprintf(fp, "shannon\n");
    break;
#ifdef ENABLE_PANINSKI
  case TRACK_PANINSKI:
    fprintf(fp, "paninski\n");
    break;
#endif /* ENABLE_PANINSKI */
  default:
    fprintf(fp, "unknown\n");
    break;
  }

  fprintf(fp, "RangeUnit: ");
  if (TRACK_TYPE(rule->type) == TRACK_BYTES)
    fprintf(fp, "bytes\n");
  else if (TRACK_TYPE(rule->type) == TRACK_PACKETS)
    fprintf(fp, "packets\n");
  else
    fprintf(fp, "unknown\n");

  for (i = 0; i < z; i++) {
    fprintf(fp, "Range: %li %li %f %f\n",
            rule->ranges[i].start, rule->ranges[i].end,
            rule->ranges[i].ent_min, rule->ranges[i].ent_max);
  }
}


/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
