#ifndef CFG_CMDLINE
#define CFG_CMDLINE

void
parse_cmdline(int argc, char *argv[]);

void
net_ent_usage(void);

#endif /* CFG_CMDLINE */
