/**
 ** @file perf_stats.c
 ** Performance statistic functions.
 ** 
 ** @author Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
 ** 
 ** @version 0.1.0
 ** 
 ** @date  Started on: Thu Nov  5 20:54:59 2009
 ** @date Last update: Tue Nov 19 22:30:23 2013
 **/


/*
 * This software is distributed under the CeCILL license.
 * See end of file for LICENSE and COPYRIGHT informations.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#ifdef ENABLE_PERF_STATS

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <nids.h>

#include "log.h"

#include "perf_stats.h"

static perf_stats_t perf_stats_g;

void
perf_stats_init(void)
{
  memset(&perf_stats_g, 0, sizeof (perf_stats_t));
  gettimeofday(&perf_stats_g.start_time, NULL);
}

void
perf_stats_open_file(const char *file)
{
  if (perf_stats_g.fp != NULL) {
    log_error("Warning: perf_stats_open_file(): "
              "a statistic file was already open. "
              "Closing the old file and reopening the new one ('%s')",
              file);
    fclose(perf_stats_g.fp);
    perf_stats_g.fp = NULL;
  }
  perf_stats_g.fp = fopen(file, "a");
  if (perf_stats_g.fp == NULL) {
    log_error("perf_stats_open_file():fopen(\"%s\"): ERROR %i: %s\n",
              file, errno, strerror(errno));
  }
}

void
perf_stats_set_timeout(time_t timeout)
{
  perf_stats_g.perf_stat_time = timeout;
}

void
perf_stats_set_call_limit(int call_limit)
{
  perf_stats_g.call_limit = call_limit;
}

void
perf_stats_set_fmt(const char *fmt)
{
  if (perf_stats_g.fmtstr != NULL)
    free(perf_stats_g.fmtstr);

  perf_stats_g.fmtstr = strdup(fmt);
}

void
perf_stats_dispose(void)
{
  if (perf_stats_g.fmtstr != NULL) {
    free(perf_stats_g.fmtstr);
    perf_stats_g.fmtstr = NULL;
  }

  if (perf_stats_g.fp != NULL) {
    fclose(perf_stats_g.fp);
    perf_stats_g.fp = NULL;
  }
}

void
perf_stats_inc_tcp_conn(void)
{
  perf_stats_g.total_tcp_conn++;
  perf_stats_g.current_tcp_conn++;
}

void
perf_stats_dec_tcp_conn(void)
{
  perf_stats_g.current_tcp_conn--;
}

void
perf_stats_tcp_pkt(void)
{
  perf_stats_g.tcp_pkt++;
}

void
perf_stats_tcp_data_pkt(void)
{
  perf_stats_g.tcp_data_pkt++;
}

void
perf_stats_tcp_timeout(void)
{
  perf_stats_g.tcp_timeout++;
  perf_stats_g.current_tcp_conn--;
}

void
perf_stats_tcp_size_limit(void)
{
  perf_stats_g.tcp_size_limit++;
  perf_stats_g.current_tcp_conn--;
}

void
perf_stats_tcp_closed(void)
{
  perf_stats_g.tcp_closed++;
  perf_stats_g.current_tcp_conn--;
}

/*
  perfstat header:
  date
  sysinfo
  net-ent version
  pagesize
*/

void
perf_stats(time_t t)
{
  if (perf_stats_g.fmtstr == NULL)
    return ;

  if ((perf_stats_g.call_count == 0) ||
      ((t - perf_stats_g.last_call_time) > perf_stats_g.perf_stat_time)) {
    fprintf_stats(perf_stats_g.fp,
                  perf_stats_g.fmtstr);
    fflush(perf_stats_g.fp);
    perf_stats_g.last_call_time = t;
  }

  perf_stats_g.call_count++;
  if (perf_stats_g.call_count >= perf_stats_g.call_limit)
    perf_stats_g.call_count = 0;
}

int
fprintf_stats(FILE *fp, const char *fmt)
{
  const char *f;
  int fc;
  int ret;

  linux_process_info_t linux_proc_info;
  int have_linux_proc_info;
#ifdef HAVE_NIDS_GETPCAPDESC
  struct pcap_stat ps;
  int have_ps;
#endif /* HAVE_NIDS_GETPCAPDESC */
  struct timeval tv;
  int have_tv;
  struct rusage ru;
  int have_ru;

  have_linux_proc_info = 0;
  have_tv = 0;
  have_ru = 0;
#ifdef HAVE_NIDS_GETPCAPDESC
  have_ps = 0;
#endif /* HAVE_NIDS_GETPCAPDESC */

  f = fmt;
  for (fc = *f; fc != '\0'; fc = *(++f)) {
    if (fc == '%') {
      fc = *(++f);
      switch (fc) {
      case '%':
        fputc('%', fp);
        break ;

        /* current number of monitored connections */
      case 'c':
        fprintf(fp, "%lu", perf_stats_g.current_tcp_conn);
        break ;

        /* total number of connection processed */
      case 'C':
        fprintf(fp, "%lu", perf_stats_g.total_tcp_conn);
        break ;

#ifdef HAVE_NIDS_GETPCAPDESC
        /* pcap_stats(): ps_drop */
      case 'd':
        if (!have_ps) {
          memset(&ps, 0, sizeof (ps));
          ret = pcap_stats(nids_getpcapdesc(), &ps);
          if (ret != 0) {
            log_error("pcap_stats(): ERROR: %s\n",
                      pcap_geterr(nids_getpcapdesc()));
            return (-1);
          }
          have_ps = 1;
        }
        fprintf(fp, "%u", ps.ps_drop);
        break ;

        /* pcap_stats(): ps_ifdrop */
      case 'D':
        if (!have_ps) {
          memset(&ps, 0, sizeof (ps));
          ret = pcap_stats(nids_getpcapdesc(), &ps);
          if (ret != 0) {
            log_error("pcap_stats(): ERROR: %s\n",
                      pcap_geterr(nids_getpcapdesc()));
            return (-1);
          }
          have_ps = 1;
        }
        fprintf(fp, "%u", ps.ps_ifdrop);
        break ;
#endif

        /* Virtual memory used (vsize) */
      case 'm':
        if (!have_linux_proc_info) {
          memset(&linux_proc_info, 0, sizeof (linux_proc_info));
          get_linux_process_info(&linux_proc_info, 0);
          have_linux_proc_info = 1;
        }
        fprintf(fp, "%lu", linux_proc_info.vsize);
        break ;

        /* Resident memory used (rss) */
      case 'M':
        if (!have_linux_proc_info) {
          memset(&linux_proc_info, 0, sizeof (linux_proc_info));
          get_linux_process_info(&linux_proc_info, 0);
          have_linux_proc_info = 1;
        }
        fprintf(fp, "%lu", linux_proc_info.rss);
        break ;

#ifdef HAVE_NIDS_GETPCAPDESC
        /* pcap_stats(): ps_recv */
      case 'r':
        if (!have_ps) {
          memset(&ps, 0, sizeof (ps));
          ret = pcap_stats(nids_getpcapdesc(), &ps);
          if (ret != 0) {
            log_error("pcap_stats(): ERROR: %s\n",
                      pcap_geterr(nids_getpcapdesc()));
            return (-1);
          }
          have_ps = 1;
        }
        fprintf(fp, "%u", ps.ps_recv);
        break ;
#endif

        /* Total process system time */
      case 's':
        if (!have_ru) {
          ret = getrusage(RUSAGE_SELF, &ru);
          if (ret != 0) {
            log_error("getrusage(): ERROR %i: %s\n",
                      errno, strerror(errno));
          }
          have_ru = 1;
        }
        fprintf(fp, "%lu.%06lu", ru.ru_stime.tv_sec, ru.ru_stime.tv_usec);
        break ;

        /* Total number of data tcp segment */
      case 'S':
        fprintf(fp, "%lu", perf_stats_g.tcp_data_pkt);
        break ;

        /* current time */
      case 't':
        if (!have_tv) {
          gettimeofday(&tv, NULL);
          have_tv = 1;
        }
        fprintf(fp, "%lu.%06lu", tv.tv_sec, tv.tv_usec);
        break ;

        /* Total number of tcp segment */
      case 'T':
        fprintf(fp, "%lu", perf_stats_g.tcp_pkt);
        break ;

        /* total process user time */
      case 'u':
        if (!have_ru) {
          ret = getrusage(RUSAGE_SELF, &ru);
          if (ret != 0) {
            log_error("getrusage(): ERROR %i: %s\n",
                      errno, strerror(errno));
          }
          have_ru = 1;
        }
        fprintf(fp, "%lu.%06lu", ru.ru_utime.tv_sec, ru.ru_utime.tv_usec);
        break ;

        /* The uptime of the program (optional) */
      case 'U':
        /* XXX: TODO */
        break ;

        /* The total number of connections close by timeout */
      case 'x':
        fprintf(fp, "%lu", perf_stats_g.tcp_timeout);
        break ;

        /* The total number of connections close by tracking limit */
      case 'y':
        fprintf(fp, "%lu", perf_stats_g.tcp_size_limit);
        break ;

        /* The total number of connections normally closed */
      case 'z':
        fprintf(fp, "%lu", perf_stats_g.tcp_closed);
        break ;

      default:
        fputc('%', fp);
        if (fc != '\0')
          fputc(fc, fp);
        break ;
      }
    }
    else if (fc == '\\') {
      fc = *(++f);
      switch (fc) {

      case '\\':
        fputc('\\', fp);
        break ;

      case 'n':
        fputc('\n', fp);
        break ;

      case 't':
        fputc('\t', fp);
        break ;

      default:
	fputc('\\', fp);
        if (fc != '\0')
          fputc(fc, fp);
	break ;
      }
    }
    else {
      fputc(fc, fp);
    }
  }

  return (0);
}


/*
** WARNING: doesn't work if the process comm name contains ")" or " "
*/
void
get_linux_process_info(linux_process_info_t *p, pid_t pid)
{
  char stat_path[20]; /* max len is '/proc/12345/stat\0' (17) */
  FILE *fp;
  int ret;

  if (pid == 0) {
    strcpy(stat_path, "/proc/self/stat");
  }
  else {
    snprintf(stat_path, sizeof (stat_path), "/proc/%d/stat", pid);
  }

  fp = fopen(stat_path, "r");
  if (fp == NULL) {
    fprintf(stderr, "fopen(%s): %s\n", stat_path, strerror(errno));
    exit(EXIT_FAILURE);
  }

  ret = fscanf(fp,
               "%d "  /* pid */
               "%s "  /* comm -- BUG: doen't work if comm contains ')' or ' ' */
               "%c "  /* state */
               "%d "  /* ppid */
               "%d "  /* pgrp */
               "%d "  /* session */
               "%d "  /* tty_nr */
               "%d "  /* tpgid */
               "%lu " /* flags */
               "%lu " /* minflt */
               "%lu " /* cminflt */ 
               "%lu " /* majflt */
               "%lu " /* cmajflt */
               "%lu " /* utime */
               "%lu " /* stime */
               "%ld " /* cutime */
               "%ld " /* cstime */
               "%ld " /* priority */
               "%ld " /* nice */
               "%*d " /* 0 */
               "%ld " /* itrealvalue */
               "%lu " /* starttime */
               "%lu " /* vsize */
               "%ld " /* rss */
               "%lu " /* rlim */
               "%lu " /* startcode */
               "%lu " /* endcode */
               "%lu " /* startstack */
               "%lu " /* kstkesp */
               "%lu " /* kstkeip */
               "%lu " /* signal */
               "%lu " /* blocked */
               "%lu " /* sigignore */
               "%lu " /* sigcatch */
               "%lu " /* wchan */
               "%lu " /* nswap */
               "%lu " /* cnswap */
               "%d "  /* exit_signal */
               "%d ", /* processor */
               &p->pid, p->comm, &p->state, &p->ppid, &p->pgrp, &p->session,
               &p->tty_nr, &p->tpgid, &p->flags, &p->minflt, &p->cminflt,
               &p->majflt, &p->cmajflt,
               &p->utime, &p->stime, &p->cutime, &p->cstime, &p->priority, &p->nice,
               &p->itrealvalue, &p->starttime, &p->vsize, &p->rss, &p->rlim,
               &p->startcode, &p->endcode, &p->startstack, &p->kstkesp, &p->kstkeip,
               &p->signal, &p->blocked, &p->sigignore, &p->sigcatch, &p->wchan,
               &p->nswap, &p->cnswap, &p->exit_signal, &p->processor);
  if (ret == EOF && ferror(fp)) {
    fprintf(stderr, "fscanf(%s): %s\n", stat_path, strerror(errno));
  }

  if (fclose(fp)) {
    fprintf(stderr, "fclose(%s): %s\n", stat_path, strerror(errno));
  }
}

#endif /* ENABLE_PERF_STATS */

/*
**  Copyright (c) 2004-2007 by Laboratoire Spécification et Vérification (LSV),
**  CNRS UMR 8643 & ENS Cachan.
**  
**  Written by Julien OLIVAIN <julien.olivain@lsv.ens-cachan.fr>
**  
**  This software is a computer program whose purpose is to detect
**  anomalous activity on cryptographic network protocols by checking
**  statistical properties.
**  
**  This software is governed by the CeCILL license under French law and
**  abiding by the rules of distribution of free software.  You can use,
**  modify and/or redistribute the software under the terms of the CeCILL
**  license as circulated by CEA, CNRS and INRIA at the following URL
**  "http://www.cecill.info".
**  
**  As a counterpart to the access to the source code and rights to copy,
**  modify and redistribute granted by the license, users are provided only
**  with a limited warranty and the software's author, the holder of the
**  economic rights, and the successive licensors have only limited
**  liability.
**  
**  In this respect, the user's attention is drawn to the risks associated
**  with loading, using, modifying and/or developing or reproducing the
**  software by the user in light of its specific status of free software,
**  that may mean that it is complicated to manipulate, and that also
**  therefore means that it is reserved for developers and experienced
**  professionals having in-depth computer knowledge.  Users are therefore
**  encouraged to load and test the software's suitability as regards
**  their requirements in conditions enabling the security of their
**  systems and/or data to be ensured and, more generally, to use and
**  operate it in the same conditions as regards security.
**  
**  The fact that you are presently reading this means that you have had
**  knowledge of the CeCILL license and that you accept its terms.
*/

/* End-of-file */
