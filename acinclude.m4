dnl some m4 macros

AC_DEFUN([AC_CHECK_LIBPCAP],
[
AC_MSG_CHECKING(for libpcap)
AC_ARG_WITH(libpcap, AS_HELP_STRING(--with-libpcap=DIR,use libpcap in DIR),
[ case "$withval" in
  no)
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libpcap not found.])
     ;;
  *)
     if test -f $withval/pcap.h; then
        PCAPINC="-I$withval"
        PCAPLIB="-L$withval -lpcap"
        AC_MSG_RESULT($withval)
     elif test -f $withval/include/pcap.h; then
        PCAPINC="-I$withval/include"
        PCAPLIB="-L$withval/lib -lpcap"
        AC_MSG_RESULT($withval)
     else
        AC_MSG_RESULT(no)
        AC_MSG_ERROR([pcap.h not found in $withval])
     fi
     ;;
  esac ],
[ if test -f /usr/include/pcap/pcap.h; then
     PCAPINC="-I/usr/include/pcap"
     PCAPLIB="-lpcap"
  elif test -f /usr/include/pcap.h; then
     PCAPLIB="-lpcap"
  elif test -f /usr/local/include/pcap.h; then
     PCAPINC="-I/usr/local/include"
     PCAPLIB="-lpcap"
  else
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libpcap not found.])
  fi
  AC_MSG_RESULT(yes) ]
)
])

AC_DEFUN([AC_CHECK_LIBNET],
[
AC_MSG_CHECKING(for libnet)
AC_ARG_WITH(libnet, AS_HELP_STRING(--with-libnet=DIR,use libnet in DIR),
[ case "$withval" in
  no)
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libnet not found.])
     ;;
  *)
     if test -f $withval/libnet.h; then
        NETINC="-I$withval"
        NETLIB="-L$withval -lnet"
        AC_MSG_RESULT($withval)
     elif test -f $withval/include/libnet.h; then
        NETINC="-I$withval/include"
        NETLIB="-L$withval/lib -lnet"
        AC_MSG_RESULT($withval)
     else
        AC_MSG_RESULT(no)
        AC_MSG_ERROR([libnet.h not found in $withval])
     fi
     ;;
  esac ],
[ if test -f /usr/include/libnet.h; then
     NETLIB="-lnet"
  elif test -f /usr/local/include/libnet.h; then
     NETINC="-I/usr/local/include"
     NETLIB="-L/usr/local/lib -lnet"
  else
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libnet not found.])
  fi
  AC_MSG_RESULT(yes) ]
)
])


AC_DEFUN([AC_CHECK_LIBNIDS],
[
AC_MSG_CHECKING(for libnids)
AC_ARG_WITH(libnids, AS_HELP_STRING(--with-libnids=DIR,use libnids in DIR),
[ case "$withval" in
  no)
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libnids not found.])
     ;;
  *)
     if test -f $withval/nids.h; then
        NIDSINC="-I$withval"
        NIDSLIB="-L$withval -lnids"
        AC_MSG_RESULT($withval)
     elif test -f $withval/include/nids.h; then
        NIDSINC="-I$withval/include"
        NIDSLIB="-L$withval/lib -lnids"
        AC_MSG_RESULT($withval)
     else
        AC_MSG_RESULT(no)
        AC_MSG_ERROR([nids.h not found in $withval])
     fi
     ;;
  esac ],
[ if test -f /usr/include/nids.h; then
     NIDSLIB="-lnids"
  elif test -f /usr/local/include/nids.h; then
     NIDSINC="-I/usr/local/include"
     NIDSLIB="-L/usr/local/lib -lnids"
  else
     AC_MSG_RESULT(no)
     AC_MSG_ERROR([libnids not found.])
  fi
  AC_MSG_RESULT(yes) ]
)
])


AC_DEFUN([AC_CHECK_MPFR],
[
AC_MSG_CHECKING(for mpfr)
AC_ARG_WITH(mpfr, AS_HELP_STRING(--with-mpfr=DIR,use mpfr in DIR),
[ case "$withval" in
  no)
     AC_MSG_RESULT(no (disabled))
     ;;
  yes)
     if test -f /usr/include/mpfr.h; then
        MPFRLIB="-lmpfr"
        AC_DEFINE([HAVE_MPFR], 1, [Set to 1 if you have the mpfr library])
     elif test -f /usr/local/include/mpfr.h; then
        MPFRINC="-I/usr/local/include"
        MPFRLIB="-L/usr/local/lib -lmpfr"
        AC_DEFINE([HAVE_MPFR], 1, [Set to 1 if you have the mpfr library])
     else
        AC_MSG_RESULT(no)
        AC_MSG_ERROR([mpfr not found.])
     fi
     AC_MSG_RESULT(yes)
     ;;
  *)
     if test -f $withval/mpfr.h; then
        MPFRINC="-I$withval"
        MPFRLIB="-L$withval -lmpfr"
        AC_DEFINE([HAVE_MPFR], 1, [Set to 1 if you have the mpfr library])
        AC_MSG_RESULT($withval)
     elif test -f $withval/include/mpfr.h; then
        MPFRINC="-I$withval/include"
        MPFRLIB="-L$withval/lib -lmpfr"
        AC_DEFINE([HAVE_MPFR], 1, [Set to 1 if you have the mpfr library])
        AC_MSG_RESULT($withval)
     else
        AC_MSG_RESULT(no)
        AC_MSG_ERROR([mpfr.h not found in $withval])
     fi
     ;;
  esac ],
[ AC_MSG_RESULT(no (disabled by default)) ]
)
])


AC_DEFUN([AC_ARG_ENABLE_STATISTICS],
[
AC_ARG_ENABLE(statistics,
AS_HELP_STRING([--enable-statistics], [enable statistics (default is off)]),
[case "${enableval}" in
    yes)
        net_entropy_statistics=true
        AC_DEFINE([ENABLE_STATISTICS], 1, [Set to 1 if STATISTICS is requested])
        ;;
    no)
        net_entropy_statistics=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-statistics)
        ;;
esac],
[net_entropy_statistics=false]
)
])


AC_DEFUN([AC_ARG_ENABLE_DOUBLE],
[
AC_ARG_ENABLE(double,
AS_HELP_STRING([--enable-double], [enable double precision (default is off)]),
[case "${enableval}" in
    yes)
        net_entropy_double=true
        AC_DEFINE([ENABLE_DOUBLE], 1, [Set to 1 if DOUBLE is requested])
        ;;
    no)
        net_entropy_double=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-double)
        ;;
esac],
[net_entropy_double=false]
)
])


AC_DEFUN([AC_ARG_ENABLE_TIMEOUT],
[
AC_ARG_ENABLE(timeout,
AS_HELP_STRING([--disable-timeout], [enable timeout (default is on)]),
[case "${enableval}" in
    yes)
        net_entropy_timeout=true
        AC_DEFINE([ENABLE_TIMEOUT], 1, [Set to 1 if TIMEOUT is requested])
        ;;
    no)
        net_entropy_timeout=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-timeout)
        ;;
esac],
[
  net_entropy_timeout=true
  AC_DEFINE([ENABLE_TIMEOUT], 1, [Set to 1 if TIMEOUT is requested])
]
)
])

AC_DEFUN([AC_WITH_DEFAULT_CONFIG_FILE],
[
AC_ARG_WITH(default-config-file,
AS_HELP_STRING([--with-default-config-file], [sets the default config file (default is $sysconfdir/net-entropy.conf)]),
[
  if test "$withval" != "" ; then
    AC_DEFINE_UNQUOTED([DEFAULT_CONFIG_FILE], "$withval", [Path to system config directory])
  fi
],
[
  dnl default action (if no --with-xxx)
  AC_DEFINE_UNQUOTED([DEFAULT_CONFIG_FILE], SYSCONFDIR "/net-entropy.conf", [Path to system config directory])
])
])

AC_DEFUN([AC_WITH_PANINSKI_CACHE_FILE],
[
AC_ARG_WITH(paninski-cache-file,
AS_HELP_STRING([--with-paninski-cache-file], [sets the paninski file (default is $localstatedir/cache/paninski.cache)]),
[
  if test "$withval" != "" ; then
    AC_DEFINE_UNQUOTED([PANINSKI_CACHE_FILE], "$withval", [Path to Paninski computation cache])
  fi
],
[
  dnl default action (if no --with-xxx)
  AC_DEFINE_UNQUOTED([PANINSKI_CACHE_FILE], LOCALSTATEDIR "/cache/paninski.cache", [Path to system config directory])
])
])

AC_DEFUN([AC_ARG_ENABLE_PANINSKI],
[
AC_ARG_ENABLE(paninski,
AS_HELP_STRING([--disable-paninski], [enable paninski entropy estimation (default is on)]),
[case "${enableval}" in
    yes)
        net_entropy_paninski=true
        AC_DEFINE([ENABLE_PANINSKI], 1, [Set to 1 if PANINSKI entropy estimation is requested])
        ;;
    no)
        net_entropy_paninski=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-paninski)
        ;;
esac],
[
  net_entropy_paninski=true
  AC_DEFINE([ENABLE_PANINSKI], 1, [Set to 1 if PANINSKI entropy estimation is requested])
]
)
])

AC_DEFUN([AC_ARG_ENABLE_PERFSTATS],
[
AC_ARG_ENABLE(perfstats,
AS_HELP_STRING([--enable-perfstats], [enable performance statistics (default is off)]),
[case "${enableval}" in
    yes)
        net_entropy_perfstats=true
        AC_DEFINE([ENABLE_PERF_STATS], 1, [Set to 1 if PERF_STATS is requested])
        ;;
    no)
        net_entropy_perfstats=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-perfstats)
        ;;
esac],
[net_entropy_perfstats=false]
)
])

AC_DEFUN([AC_ARG_ENABLE_ALT_ENTROPY_CODE],
[
AC_ARG_ENABLE(alt-entropy-code,
AS_HELP_STRING([--enable-alt-entropy-code], [enable alternate entropy code (default is off)]),
[case "${enableval}" in
    yes)
        net_entropy_alt_code=true
        AC_DEFINE([ENABLE_ALT_ENTROPY_CODE], 1, [Set to 1 if ALT_ENTROPY_CODE is requested])
        ;;
    no)
        net_entropy_alt_code=false
        ;;
    *)
        AC_MSG_ERROR(bad value ${enableval} for --enable-alt-entropy-code)
        ;;
esac],
[net_entropy_alt_entropy_code=false]
)
])

AC_DEFUN([AC_WITH_MPFR_PRECISION],
[
AC_ARG_WITH(mpfr-precision,
AS_HELP_STRING([--with-mpfr-precision], [sets the MPFR precision in bits (default is 512)]),
[
  if test "$withval" != "" ; then
    AC_DEFINE_UNQUOTED([MPFR_PRECISION], $withval, [MPFR precision])
  fi
],
[
  dnl default action (if no --with-xxx)
  AC_DEFINE_UNQUOTED([MPFR_PRECISION], 512, [MPFR precision])
])
])
