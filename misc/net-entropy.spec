%define name	net-entropy
%define version 1.0
%define release 1
%define prefix  /

Summary: An entropy checker for ciphered network connections
Name: %{name}
Version: %{version}
Release: %{release}
License: CeCILL
Group: System Environment/Daemons

Source: net-entropy-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}-%{release}-root

%description
* Checks that statistical entropy property of ciphered data seems correct
* Detects attacks on cryptographic software that create low entropy
  network traffic (e.g. a shellcode)

%prep

%setup

%build
%configure --with-default-config-file=/etc/net-entropy.conf
make

%install
mkdir -p $RPM_BUILD_ROOT/etc/init.d
cp misc/net-entropy-sysv $RPM_BUILD_ROOT/etc/init.d/net-entropy
%makeinstall

%files
%defattr(-,root,root)
%attr(755,root,root) %{prefix}/usr/bin/net-entropy
%attr(755,root,root) %{prefix}/etc/init.d/net-entropy
%attr(644,root,root) %{prefix}/etc/net-entropy.conf
%attr(644,root,root) %{prefix}/usr/share/net-entropy/protospec/*
%doc AUTHORS ChangeLog COPYING* NEWS README

%clean
rm -r $RPM_BUILD_ROOT

%post
/sbin/chkconfig --add net-entropy

%preun
if [ $1 -eq 0 ]; then
        /sbin/service net-entropy stop &>/dev/null || :
        /sbin/chkconfig --del net-entropy
fi

%postun
/sbin/service %{name} condrestart &>/dev/null || :

%changelog
