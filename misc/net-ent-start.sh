#! /bin/sh

NETWORK="192.168.0.0/24"
IFACE="lo"

ulimit -H -v 262144
ulimit -S -v 262144

./net-ent \
-i $IFACE \
-f "(dst net $NETWORK
       and
         (dst port 22 or dst port 443 or dst port 993 or dst port 995))
    or
    (src net $NETWORK
       and
         (src port 22 or src port 443 or src port 993 or src port 995))" \
-p "<=>22:3000B:7.0"  \
-p "<=>443:1500B:7.0" \
-p "<=>993:1500B:7.0" \
-p "<=>995:1500B:7.0"

