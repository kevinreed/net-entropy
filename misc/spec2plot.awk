#!/usr/bin/awk -f

BEGIN {
  CONVFMT="%.10g";

# print header:

  print "# Plot generated with Net-Entropy spec2plot.awk script";
  print "# first column is rangeunit (byte or packet)";
  print "# second column is the valid entropy range (min and max)";
  print "# third column is the entropy variance";
  print "# fourth column is the mean entropy of the range";
  print "#";
  print "# gnuplot with:";
  print "# set grid";
  print "# set logscale x 2";
  print "# set key bottom right";
  print "# plot 'file' using 1:2 with filledcurve title \"Valid entropy range\", \\";
  print "#      '' using 1:3 with lines title \"Entropy variance\", \\";
  print "#      '' using 1:4 with lines title \"Mean entropy\"";
  print "#";
  print "#";
}

$1 ~ /Range:/ && $4 !~ /[Nn][Aa][Nn]/ && $5 !~ /[Nn][Aa][Nn]/ {
  print $2   "\t" $4 "\t" $6-$7 "\t" $6;
  print $3+1 "\t" $4 "\t" $6-$7 "\t" $6;
  print $3+1 "\t" $5 "\t" $6+$7;
  print $2   "\t" $5 "\t" $6+$7;
  print $2   "\t" $4 "\t" $6-$7;
  # Older version of GNUPlot needed NaN (not-a-number) values
  # to break a line drawing.  It appears newer version (>= 4.0)
  # only needs an empty data line.
  #print "nan\tnan\tnan\tnan";
  print "\n";
}
