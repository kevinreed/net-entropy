#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define PANINSKI_CACHE_MAGIC "PAN"

typedef struct paninski_cache_hdr_s paninski_cache_hdr_t;
struct paninski_cache_hdr_s
{
  char magic[4]; /* should be "PAN" 0 terminated string */
  int version;
  size_t sizeof_float;
  size_t cache_size;
};

static void
fprintf_paninski_header(FILE *fp, const paninski_cache_hdr_t *h)
{
  fprintf(fp, "# magic:         %3s\n", h->magic);
  fprintf(fp, "# version:       %u\n", h->version);
  fprintf(fp, "# sizeof_float:  %u\n", h->sizeof_float);
  fprintf(fp, "# cache_size:    %u\n", h->cache_size);
}

static int
read_value(FILE *cache, const size_t float_size, double *val)
{
  float fval;
  double dval;
  size_t s;

  if (float_size == sizeof (float)) {
    s = fread(&fval, sizeof (float), 1, cache);
    *val = fval;
  }
  else if (float_size == sizeof (double)) {
    s = fread(&dval, sizeof (float), 1, cache);
    *val = dval;
  }
  else {
    fprintf(stderr, "ERROR: unsupported float size %i\n", float_size);
    return (-1);
  }

  if (s != 1) {
    if (feof(cache)) {
      fprintf(stderr, "ERROR: unexpected end of file.\n");
    }
    else {
      fprintf(stderr, "ERROR: fread(): error %i: %s\n",
              errno, strerror(errno));
    }

    return (-1);
  }

  return (0);
}

static void
dump_cache(FILE *out, FILE *cache)
{
  int i;
  size_t s;
  paninski_cache_hdr_t hdr;

  s = fread(&hdr, sizeof (hdr), 1, cache);
  if (s != 1) {
    if (feof(cache)) {
      fprintf(stderr, "ERROR: unexpected end of file\n");
    }
    else {
      fprintf(stderr, "ERROR: fread(): error %i: %s\n",
              errno, strerror(errno));
    }
    return ;
  }

  if (strcmp(hdr.magic, PANINSKI_CACHE_MAGIC)) {
    fprintf(stderr, "ERROR: bad magic value (expected '%s', read '%3s')\n",
            PANINSKI_CACHE_MAGIC,
            hdr.magic);
  }

  fprintf_paninski_header(stdout, &hdr);

  for (i = 0; i < hdr.cache_size; i++) {
    int ret;
    double v;
    v = 0.0;
    ret = read_value(cache, hdr.sizeof_float, &v);
    if (ret != 0) {
      if (feof(cache)) {
        fprintf(stderr, "ERROR: while reading value: "
                "unexpected end of file.\n");
        return ;
      }
      else {
        fprintf(stderr, "ERROR: while reading value: error %i: %s\n",
                errno, strerror(errno));
      }
    }
    fprintf(out, "%i\t%10.8f\n", i, v);
  }

}

static void
dump_cache_file(const char *fname)
{
  FILE *fp;

  fp = fopen(fname, "r");
  if (fp == NULL) {
    fprintf(stderr, "ERROR: fopen('%s'): error %i: %s\n",
            fname, errno, strerror(errno));
    return ;
  }

  dump_cache(stdout, fp);

  fclose(fp);
}

int
main(int argc, char *argv[])
{
  if (argc != 2) {
    fprintf(stderr, "usage: %s <cache-file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  dump_cache_file(argv[1]);

  return (0);
}
