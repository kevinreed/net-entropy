BEGIN {

  merge = 0;

  for (i = 1; i < ARGC; i++) {
    if (ARGV[i] == "--merge")
      merge = 1;
    else if (ARGV[i] ~ /^-+/) {
      e = sprintf("%s: unrecognized option: %s", ARGV[0], ARGV[i]);
      print e > "/dev/stderr";
    } else {
      break;
    }
    delete ARGV[i];
  }

  zone1 = 0;
  zone2 = 0;
}

$1 ~ /Zone:/ && $4 !~ /[Nn][Aa][Nn]/ && $5 !~ /[Nn][Aa][Nn]/ && file == 1 {
  start1[zone1] = $2;
  if (start1[zone1] <= end1[zone1-1]) {
    print "error";
  }
  end1[zone1] = $3;
  if (end1[zone1] < start1[zone1]) {
    print "error";
  }
  min1[zone1] = $4;
  max1[zone1] = $5;

  zone1++;
}

$1 ~ /Zone:/ && file == 2 {
  start2[zone2] = $2;
  if (start2[zone2] <= end2[zone2-1]) {
    print "error";
  }
  end2[zone2] = $3;
  if (end2[zone2] < start2[zone2]) {
    print "error";
  }
  if (merge && $4 !~ /[Nn][Aa][Nn]/) {
    min2[zone2] = $4;
  }
  else {
    min2[zone2] = 8.0;
  }

  if (merge && $5 !~ /[Nn][Aa][Nn]/) {
    max2[zone2] = $5;
  }
  else {
    max2[zone2] = 0.0;
  }

  zone2++;
}

$1 !~ /Zone:/ && file == 2 {
  print $0;
}

FNR == 1 {
  file = file + 1;
}

END {
  for (j = 0; j < zone2; j++) {
    for (i = 0; i < zone1; i++) {

      if ( (start1[i] >= start2[j] && start1[i] <= end2[j]) \
        || (end[i]    >= start2[j] && end1[i]   <= end2[j]) \
        || (start1[i] <= start2[j] && end1[i]    >= end2[j])) {
        if (max1[i] > max2[j]) {
          max2[j] = max1[i];
        }
        if (min1[i] < min2[j]) {
          min2[j] = min1[i];
        }
      }

    }
    if (min2[j] <= max2[j])
      print "Zone: " start2[j] " " end2[j] " " min2[j] " " max2[j];
    else
      print "Zone: " start2[j] " " end2[j] " nan nan";
  }
}
