
set grid
set logscale x 2
set key bottom right

set xlabel "Data size (bytes)"
set ylabel "Entropy (bits per byte)"

set arrow 1 from 200,4.25 to 200,5.8 front filled lt 3 lw 2
set label 1 "Out of range" at 210,4.25 left front

set arrow 2 from 350,4.75 to 350,6.12 front filled lt 3 lw 2
set label 2 "Re-enter range" at 360,4.75 left front

set arrow 3 from 2500,5.25 to 2500,7.1 front filled lt 3  lw 2
set label 3 "Rising entropy alarm" at 2550,5.25 left front

set arrow 4 from 6000,5.7 to 6000,7.7 front filled lt 3 lw 2
set label 4 "Falling entropy alarm" at 6100,5.7 left front

set arrow 5 from 58000,6.75 to 58000,7.9 front filled lt 3 lw 2
set label 5 "End of connection" at 55000,6.75 right front

set style fill solid 0.1

set term pdf noenhanced fname "Helvetica" fsize 10
set output "net-ent-msgs.pdf"
plot \
  "net-ent-msgs-ranges.dat" with filledcurve lw 2 title "Valid ranges", \
  "net-ent-msgs-data.dat" with linespoints lw 2 pt 2 title "A tcp connection"

set arrow 1 from 200,4.25 to 200,5.8 front filled lt 3 lw 1
set label 1 "Out of range" at 210,4.25 left front

set arrow 2 from 350,4.75 to 350,6.12 front filled lt 3 lw 1
set label 2 "Re-enter range" at 360,4.75 left front

set arrow 3 from 2500,5.25 to 2500,7.1 front filled lt 3  lw 1
set label 3 "Rising entropy alarm" at 2550,5.25 left front

set arrow 4 from 6000,5.7 to 6000,7.7 front filled lt 3 lw 1
set label 4 "Falling entropy alarm" at 6100,5.7 left front

set arrow 5 from 58000,6.75 to 58000,7.9 front filled lt 3 lw 1
set label 5 "End of connection" at 55000,6.75 right front

set style fill solid 0.1

set term png font Vera 7 size 480,360
set output "net-ent-msgs.png"
plot \
  "net-ent-msgs-ranges.dat" with filledcurves lw 1 title "Valid ranges", \
  "net-ent-msgs-data.dat" with linespoints lw 1 pt 2 title "A tcp connection"
