
set title "Network data entropy of plain text connections"

set grid
set xlabel "Data size (Bytes)"
set ylabel "Entropy (bits per Byte)"

set term png font Vera 7 size 480,360
set output "plain-data.png"
plot [0:10000] [0:8] "smtp.dat"   using 2:3 with linespoints title "smtp", \
                     "telnet.dat" using 2:3 with linespoints title "telnet", \
                     "http.dat"   using 2:3 with linespoints title "http"

set term postscript eps noenhanced 10
set output "plain-data.eps"
plot [0:10000] [0:8] "smtp.dat"   using 2:3 with linespoints title "smtp", \
                     "telnet.dat" using 2:3 with linespoints title "telnet", \
                     "http.dat"   using 2:3 with linespoints title "http"



set term pdf noenhanced fname "Helvetica" fsize 6
set output "plain-data.pdf"
plot [0:10000] [0:8] "smtp.dat"   using 2:3 with linespoints title "smtp", \
                     "telnet.dat" using 2:3 with linespoints title "telnet", \
                     "http.dat"   using 2:3 with linespoints title "http"
