
set title "Network data entropy of a normal https connection"

# set logscale x 2

set grid
set key bottom right
set xlabel "Data size (Bytes)"
set ylabel "Entropy (bits per Byte)"

set arrow 1 from 150,5.5 to 1900,5.5 front size 90,30 filled lt 3
set label 1 "Normal key exchange" at 1000,5.2 center front

set arrow 2 from 2000,5.5 to 5100,5.5 lt 3 nohead
set label 2 "Ciphered data" at 3500,5.2 center front

set arrow 3 from 5100,5.5 to 5600,5.5 front size 90,30 filled lc 3 lt 0

#set arrow 3 from 1800,5 to 1800,6.6 front size 90,30 filled lt 3
#set label 3 "Attack" at 1800,4.8 center front

set term png font Vera 7 size 480,360
set output "normal-https.png"
plot [1:6000] [0:8] "normal-https.dat" using 2:3 with linespoints \
                     title "Cumulative packet entropy", \
                        "perfect-avg.dat" using 1:2 with lines \
                        title "Average entropy of a random source" lc 0


set term postscript eps noenhanced 10
set output "normal-https.eps"
plot [1:6000] [0:8] "normal-https.dat" using 2:3 with linespoints \
                     title "Cumulative packet entropy"



set term pdf noenhanced fname "Helvetica" fsize 6
set output "normal-https.pdf"
plot [1:6000] [0:8] "normal-https.dat" using 2:3 with linespoints \
                     title "Cumulative packet entropy"
