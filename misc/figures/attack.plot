
set title "Network data entropy of the OpenSSL/mod_SSL attack"

set grid
set key bottom right
set xlabel "Data size (Bytes)"
set ylabel "Entropy (bits per Byte)"

set arrow 1 from 50,4 to 1750,4 front size 90,30 filled lt 3
set label 1 "Normal key exchange" at 800,3.7 center front

set arrow 2 from 1850,4 to 5000,4 front size 90,30 filled lt 3
set label 2 "Malicious traffic" at 3300,3.7 center front

set arrow 3 from 1800,5 to 1800,6.6 front size 90,30 filled lt 3
set label 3 "Attack" at 1800,4.8 center front

set term png font Vera 7 size 480,360
set output "attack.png"
plot [0:6000] [0:8] "attack.dat" using 2:3 with linespoints \
                    title "Cumulative packet entropy of the attack", \
                    "normal-https.dat" using 2:3 with linespoints \
                    title "Cumulative packet entropy of normal HTTPS" lc 0


set term postscript eps enhanced 10
set output "attack.eps"
plot [0:6000] [0:8] "attack.dat" using 2:3 with linespoints \
                     title "Cumulative packet entropy"



set term pdf noenhanced fname "Helvetica" fsize 6
set output "attack.pdf"
plot [0:6000] [0:8] "attack.dat" using 2:3 with linespoints \
                     title "Cumulative packet entropy"
