#
# On some distributions like Fedora 11, you may need to
# define the GDFONTPATH variable for rendering graphs.
# Example:
# GDFONTPATH=/usr/share/fonts/bitstream-vera gnuplot file.plot
#

log2(x)=log(x)/log(2)

set title "Average statistical entropy estimated from small random messages"

set grid
set logscale x 2
set key right bottom
set xlabel "Data size (Bytes, logarithmic scale)"
set ylabel "Entropy (bits per Byte)"

# plot [1:6000]  "perfect-avg.dat" using 1:2 with lines \
#                 title "Entropy"

set term png font Vera 7 size 480,360
set output "perfect-avg.png"
plot [1:65536] [0:8.2]  8.0 notitle lt 3, \
                        log2(x) lt 2, \
                        "perfect-avg.dat" using 1:2 with lines \
                        title "Average Entropy" lt 1 lw 2

set term postscript eps noenhanced 10
set output "perfect-avg.eps"
plot [1:6000] [0:8.1] "perfect-avg.dat" using 1:2 with lines \
                     title "Entropy"

set term pdf noenhanced fname "Helvetica" fsize 6
set output "perfect-avg.pdf"
plot [1:65536] [0:8.5] "perfect-avg.dat" using 1:2 with lines \
                     title "Average entropy", \
                       log2(x) title "Asymptote y=log2(x)", \
                       8 title "Asymptote y=8"

