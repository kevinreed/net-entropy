#! /bin/sh

LIM=4000

mkdir png eps plot 2> /dev/null

for f in *.dat ; do
  GRAPH=$(echo $f | sed 's/.dat$//')
  FROM=$(echo $f | sed 's/^[^-]*-\([0-9.:]*\).*\.dat$/\1/')
  TO=$(echo $f | sed 's/^[^-]*-[0-9.:]*[<-=>]*\([0-9.:]*\)\.dat$/\1/') 
  gnuplot <<EOF
#! /usr/bin/gnuplot

print "Proceeding $GRAPH"

set grid
# set ytics 1
set title "Entropy in data from $FROM to $TO"
set xlabel "Network data (in bytes)"
set ylabel "Entropy"

set term postscript eps enhanced 10
print ">> Rendering $GRAPH.eps"
set output "eps/$GRAPH.eps"
plot [0:$LIM] [0:8] "$GRAPH.dat" using 2:3 title "Entropy" with linespoints

set term png small
print ">> Rendering $GRAPH.png"
set output "png/$GRAPH.png"
plot [0:$LIM] [0:8] "$GRAPH.dat" using 2:3 title "Entropy" with linespoints

EOF

cat > ./plot/$GRAPH.plot << EOF
#! /usr/bin/gnuplot

print "Displaying $GRAPH"

set grid
# set ytics 1
set title "Entropy in data from $FROM to $TO"
set xlabel "Network data (in bytes)"
set ylabel "Entropy"
plot [0:$LIM] [0:8] "$GRAPH.dat" using 2:3 title "Entropy" with linespoints
pause -1  "Hit return to continue"

EOF

#  gnuplot $GRAPH.plot

done
