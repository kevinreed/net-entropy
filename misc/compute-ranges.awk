#!/usr/bin/awk -f
#
# Computes ranges from a datafile generated by Net-Entropy
# in statistics mode.

function log2(x) {
  return log(x)/log(2);
}

BEGIN {
  CONVFMT="%.10g"
  merge = 0;

  for (i = 1; i < ARGC; i++) {
    if (ARGV[i] == "--merge")
      merge = 1;
    else if (ARGV[i] ~ /^-+/) {
      e = sprintf("%s: unrecognized option: %s", ARGV[0], ARGV[i]);
      print e > "/dev/stderr";
    } else {
      break;
    }
    delete ARGV[i];
  }

  pt = 0;
  zone2 = 0;
}

file == 1 && $1 !~ /[Nn][Aa][Nn]/ && $2 !~ /[Nn][Aa][Nn]/ {
  x[pt] = $1;
  y[pt] = $2;
  pt++;
}

$1 ~ /Range:/ && file == 2 {
  start2[zone2] = $2;
  if (start2[zone2] <= end2[zone2-1]) {
    print "error";
  }
  end2[zone2] = $3;
  if (end2[zone2] < start2[zone2]) {
    print "error";
  }
  if (merge && $4 !~ /[Nn][Aa][Nn]/) {
    min2[zone2] = $4;
  }
  else {
    min2[zone2] = 8.0;
  }

  if (merge && $5 !~ /[Nn][Aa][Nn]/) {
    max2[zone2] = $5;
  }
  else {
    max2[zone2] = 0.0;
  }

  zone2++;
}

FNR == 1 {
  file = file + 1;
}

END {
  print "# Range: start end min_ent max_ent avg var points pointperunit"
  for (j = 0; j < zone2; j++) {
    sum=0.0;
    sumn=0.0;
    var=0.0;
    for (i = 0; i < pt; i++) {
      if (x[i] >= start2[j] && x[i] <= end2[j]) {
        if (y[i] < min2[j])
          min2[j] = y[i];
        if (y[i] > max2[j])
          max2[j] = y[i];
        sum += y[i];
        sumn += 1.0;
      }
    }
    if (sumn > 0) {
      avg = sum/sumn;
      for (i = 0; i < pt; i++) {
        if (x[i] >= start2[j] && x[i] <= end2[j]) {
          tmp = avg - y[i];
          var += tmp * tmp;
        }
      }
      var = var / (sumn - 1.0);
      if (start2[j] < 256 && min2[j] > log2(start2[j])) {
        print "# Correcting minimal entropy of range " start2[j] " " end2[j] " from " min2[j] " to " log2(start2[j]);
        min2[j] = log2(start2[j]);
      }
      if (end2[j] < 256 && max2[j] > log2(end2[j])) {
        print "# Correcting maximal entropy of range " start2[j] " " end2[j] " from " max2[j] " to " log2(end2[j]);
        max2[j] = log2(end2[j]);
      }
      print "Range: " start2[j] " " end2[j] " " min2[j] " " max2[j] " " avg " " var " " sumn " " sumn / (end2[j] - start2[j] + 1.0);
    }
    else {
      print "# Range: " start2[j] " " end2[j] " nan nan nan 0 0 0";
    }
  }
}
